package ru.omgtu.art.test;

import com.MAVLink.MAVLinkPacket;

import ru.omgtu.art.projectbot.communication.IConnector;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;

public class TestConnectorClass implements IConnector, ITestClass{

	private MethodName last_call_method_name;
	private Object last_call_method_data;
	private boolean is_connected=false;
	
	@Override
	public void connect(String port_name, int baud_rate, boolean mavlink_protocol, MsgInterpeter mint) {
		is_connected=true;
		setLastCallMethod(MethodName.IConnector_connect);
	}

	@Override
	public void disconnect() {
		is_connected=false;
		setLastCallMethod(MethodName.IConnector_disconnect);
	}

	@Override
	public void sendMsg(String msg) throws NoOpenConnectionException {
		if(!isConnected())
			throw new NoOpenConnectionException();
		setLastCallMethod(MethodName.Iconnector_sendMsg);
		setLastCallMethodData(msg);
	}

	@Override
	public void sendMsg(int[] msg) throws NoOpenConnectionException {
		if(!isConnected())
			throw new NoOpenConnectionException();
		setLastCallMethod(MethodName.Iconnector_sendMsg);
		setLastCallMethodData(msg);
	}

	@Override
	public void sendMsg(MAVLinkPacket msg) throws NoOpenConnectionException {
		if(!isConnected())
			throw new NoOpenConnectionException();
		setLastCallMethod(MethodName.Iconnector_sendMsg);
		setLastCallMethodData(msg);
	}

	public MethodName getLastCallMethod() {
		return last_call_method_name;
	}

	private void setLastCallMethod(MethodName last_call_method) {
		this.last_call_method_name = last_call_method;
	}

	@Override
	public MethodName getLastCallMethodName() {
		return last_call_method_name;
	}

	@Override
	public void clrLastCallMethodInfo() {
		last_call_method_name=MethodName.None;
		last_call_method_data=null;
	}

	@Override
	public Object getLastCallMethodData() {
		return last_call_method_data;
	}

	private void setLastCallMethodData(Object last_call_method_data) {
		this.last_call_method_data = last_call_method_data;
	}

	@Override
	public boolean isConnected() {
		return is_connected;
	}

}
