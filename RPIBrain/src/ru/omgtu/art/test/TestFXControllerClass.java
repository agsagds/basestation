package ru.omgtu.art.test;

import javafx.scene.control.Alert.AlertType;
import ru.omgtu.art.projectbot.IBaseStantion;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.graphic.IFXController;

public class TestFXControllerClass implements IFXController, ITestClass{

	private MethodName last_call_method_name;
	private Object last_call_method_data;
	
	public TestFXControllerClass() {
		setLastCallMethodName(MethodName.None);
	}
	
	@Override
	public void init(IBaseStantion base_stantion) {
		setLastCallMethodName(MethodName.IFXController_init);		
	}

	@Override
	public void dispose() {
		setLastCallMethodName(MethodName.IFXController_dispose);	
	}

	@Override
	public void refreshHeartBeatIndicator() {
		setLastCallMethodName(MethodName.IFXController_refreshHeartBeatIndicator);	
	}

	@Override
	public void refreshImView() {
		setLastCallMethodName(MethodName.IFXController_refreshImView);	
	}

	@Override
	public void refreshBatteryPb() {
		setLastCallMethodName(MethodName.IFXController_refreshBatteryPb);	
	}

	@Override
	public void setLabelText(String text, boolean is_err) {
		setLastCallMethodName(MethodName.IFXController_setLabelText);	
	}

	@Override
	public void setRoverWorkTime(long mills) {
		setLastCallMethodName(MethodName.IFXController_setRoverWorkTime);	
	}

	@Override
	public void setActivePathMark(int path_mark_number) {
		setLastCallMethodName(MethodName.IFXController_setActivePathMark);	
	}

	public MethodName getLastCallMethodName() {
		return last_call_method_name;
	}

	private void setLastCallMethodName(MethodName last_call_method) {
		this.last_call_method_name = last_call_method;
	}

	@Override
	public void clrLastCallMethodInfo() {
		last_call_method_data=null;
		last_call_method_name=MethodName.None;
		
	}

	@Override
	public Object getLastCallMethodData() {
		return this.last_call_method_data;
	}

	@Override
	public void showErrorAlert(String message) {
		setLastCallMethodName(MethodName.IFXController_showErrorAlert);
		last_call_method_data=message;		
	}

	@Override
	public void showWarningAlert(String message) {
		setLastCallMethodName(MethodName.IFXController_showWarningAlert);
		last_call_method_data=message;
	}

	@Override
	public void showInfoAlert(String message) {
		setLastCallMethodName(MethodName.IFXController_showInfoAlert);
		last_call_method_data=message;
	}

	

}
