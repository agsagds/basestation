package ru.omgtu.art.test;

public interface ITestClass {
	public MethodName getLastCallMethodName();
	public void clrLastCallMethodInfo();
	public Object getLastCallMethodData();
}
