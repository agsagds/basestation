package ru.omgtu.art.test;

public enum MethodName {
	None,
	IConnector_connect,
	IConnector_disconnect,
	Iconnector_sendMsg,
	IFXController_init,
	IFXController_dispose,
	IFXController_refreshHeartBeatIndicator,
	IFXController_refreshImView,
	IFXController_refreshBatteryPb,
	IFXController_setLabelText,
	IFXController_setRoverWorkTime,
	IFXController_setActivePathMark,
	IFXController_showErrorAlert,
	IFXController_showWarningAlert,
	IFXController_showInfoAlert
}
