package ru.omgtu.art.test;

import ru.omgtu.art.mapapi.Rover;
import ru.omgtu.art.projectbot.IBaseStantion;
import ru.omgtu.art.projectbot.MissionData;
import ru.omgtu.art.projectbot.SystemStatus;
import ru.omgtu.art.projectbot.WorkConfiguration;
import ru.omgtu.art.projectbot.communication.HeartBeatSender;
import ru.omgtu.art.projectbot.communication.IConnector;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.graphic.FXRunner;
import ru.omgtu.art.projectbot.graphic.IFXController;
import ru.omgtu.art.projectbot.graphic.RunActionEnum;

public class TestBaseStantionClass implements IBaseStantion {
	private IFXController controller;
	private volatile Rover rover;
	private volatile SystemStatus status;
	private volatile MissionData mission_data;
	private WorkConfiguration work_config;
	private FXRunner fxrunner;
	
	@Override
	public SystemStatus getStatus() {
		return status;
	}
	@Override
	public Rover getRover() {
		return rover;
	}

	@Override
	public MsgInterpeter getMsgInterpreter() {
		return null;
	}

	@Override
	public IFXController getFXController() {
		return controller;
	}

	@Override
	public MissionData getMissionData() {
		return mission_data;
	}

	@Override
	public WorkConfiguration getWorkConfig() {
		return work_config;
	}

	@Override
	public void init(IConnector connector, IFXController fxcontroller) {
		//init work configuration
		work_config=new WorkConfiguration();
		//create system status
		status=new SystemStatus();
		//init mission data
		mission_data=new MissionData();
		//create rover
		rover=new Rover(55.026f, 73.29f, false, 0);	
		//set GUI controller
		controller=fxcontroller;
	}
	@Override
	public void runGUIAction(RunActionEnum action, Object parameter) {
		fxrunner=new FXRunner(action, controller, parameter);
	}
	
	public FXRunner getFXRunner()
	{
		return fxrunner;
	}
}
