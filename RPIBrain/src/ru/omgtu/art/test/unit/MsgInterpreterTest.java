package ru.omgtu.art.test.unit;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.common.*;
import com.MAVLink.enums.*;

import ru.omgtu.art.mapapi.MapMark;
import ru.omgtu.art.projectbot.ControlMode;
import ru.omgtu.art.projectbot.Utils;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;
import ru.omgtu.art.projectbot.graphic.RunActionEnum;
import ru.omgtu.art.test.MethodName;
import ru.omgtu.art.test.TestBaseStantionClass;
import ru.omgtu.art.test.TestConnectorClass;
import ru.omgtu.art.test.TestFXControllerClass;

public class MsgInterpreterTest {

	private static MsgInterpeter mint;
	private static TestFXControllerClass controller;
	private static TestConnectorClass connector;
	private static TestBaseStantionClass stantion;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		controller=new TestFXControllerClass();
		connector=new TestConnectorClass();
		stantion=new TestBaseStantionClass();
		stantion.init(connector, controller);
	}
	
	@Before
	public void resetITestClasses()
	{
		mint=new MsgInterpeter(100, 100, 0, (short)100, (short)100, stantion, connector);
		connector.clrLastCallMethodInfo();
		connector.disconnect();
		controller.clrLastCallMethodInfo();
	}

	@Test
	public void openConnection() {
		assertTrue(mint.openConnection("Test Port"));
		assertEquals(connector.getLastCallMethod(),MethodName.IConnector_connect);
		assertTrue(connector.isConnected());
	}
	
	@Test
	public void disposing()
	{
		mint.openConnection("Test Port");
		mint.closeConnection();
		assertEquals(connector.getLastCallMethod(), MethodName.IConnector_disconnect);
		assertFalse(connector.isConnected());
	}
	
	@Test
	public void getConnector()
	{
		assertTrue(mint.getConnector()==connector);
	}
	
	@Test
	public void generateHeartBeat()
	{
		try{
			mint.generateHeartBeat();
			fail("No generate Exception");
		} catch (NoOpenConnectionException ex)
		{
			mint.openConnection("Test Port");
			try
			{
				mint.generateHeartBeat();
				assertEquals(connector.getLastCallMethod(), MethodName.Iconnector_sendMsg);
				int msg_id=((MAVLinkPacket)connector.getLastCallMethodData()).msgid;
				assertEquals("incorrect msg id", msg_id, msg_heartbeat.MAVLINK_MSG_ID_HEARTBEAT);
			} catch (NoOpenConnectionException ex1)
			{
				fail();
			}
		}
	}
	
	
	
	@Test
	public void interpretMavMsgMissionRequest()
	{
		msg_mission_request msg=new msg_mission_request();
		msg.compid=100;
		msg.seq=100;
		msg.sysid=100;
		msg.target_component=100;
		msg.target_system=100;
		stantion.getStatus().setNextWaypointId(-1);
		stantion.getStatus().clrRecivedResponseStatus();
		mint.responseInterpret(msg.pack());
		assertEquals("Incorrect interpret", stantion.getStatus().getNextWaypointId(), msg.seq);
		assertTrue("Incorrect interpret", stantion.getStatus().isResponseRecived());
	}
	
	@Test
	public void interpretMavMsgMissionAck()
	{
		msg_mission_ack msg=new msg_mission_ack();
		msg.compid=100;		
		msg.sysid=100;
		msg.target_component=100;
		msg.target_system=100;		
		for(int i=0; i< MAV_MISSION_RESULT.MAV_MISSION_RESULT_ENUM_END;i++)
		{
			msg.type=(short) i;
			stantion.getStatus().setNextWaypointId(100500);
			stantion.getStatus().clrRecivedResponseStatus();
			stantion.getStatus().clrMavErrStatus();
			mint.responseInterpret(msg.pack());
			assertEquals("Incorrect interpret", stantion.getStatus().getNextWaypointId(), -1);
			assertTrue("Incorrect interpret", stantion.getStatus().isResponseRecived());
			assertEquals("Incorrect interpret", stantion.getStatus().isMavErr(), i!=MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED);
		}
	}
	
	@Test
	public void interpretMavMsgScaledIMU()
	{
		msg_scaled_imu msg=new msg_scaled_imu();
		msg.compid=100;		
		msg.sysid=100;
		//zmag - move direction in degrees. Value is 0 - on North
		msg.zmag=123;	
		//xacc - body x angle in degrees
		//yacc - body y angle in degrees
		msg.xacc=321;
		msg.yacc=1234;		
		mint.responseInterpret(msg.pack());
		assertTrue("Incorrect interpret", stantion.getRover().getMove_direction()==msg.zmag);
		assertTrue("Incorrect interpret", stantion.getRover().getX_body_angle()==msg.xacc);
		assertTrue("Incorrect interpret", stantion.getRover().getY_body_angle()==msg.yacc);
		for(int i=-360;i<360;i++)
		{
			//zacc - head direction in degrees [-180; 180]. Value is 0 - on North
			msg.zacc=(short) i;
			mint.responseInterpret(msg.pack());
			if(i<-180)
				assertTrue("Incorrect interpret", stantion.getRover().getZ_head_angle()==-180);
			else if(i>180)
				assertTrue("Incorrect interpret", stantion.getRover().getZ_head_angle()==180);
			else
				assertTrue("Incorrect interpret", stantion.getRover().getZ_head_angle()==msg.zacc);
		}
	}
	
	@Test
	public void interpretMavMsgMissionCurrent()
	{
		msg_mission_current msg=new msg_mission_current();
		msg.compid=100;		
		msg.sysid=100;
		msg.seq=123;
		mint.responseInterpret(msg.pack());
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.SET_ACTIVE_PATH_MARK);
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getParameter(), msg.seq);
	}

	@Test
	public void interpretMavMsgHeartBeat()
	{
		msg_heartbeat msg=new msg_heartbeat();
		msg.compid=100;		
		msg.sysid=100;
		msg.system_status=MAV_STATE.MAV_STATE_CRITICAL;
		String text="������! ���������� �������� ����������. �������� ������ �����";
		stantion.getStatus().clrRoverErrorStatus();
		mint.responseInterpret(msg.pack());
		assertTrue("Incorrect interpret", stantion.getStatus().isRoverError());
		assertEquals("Incorrect interpret", stantion.getStatus().getHeartbeatRecivedLastTime(), System.currentTimeMillis());
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.SET_ERROR_TEXT);
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getParameter(), text);
		msg.system_status=MAV_STATE.MAV_STATE_ACTIVE;
		stantion.getStatus().clrRoverErrorStatus();
		mint.responseInterpret(msg.pack());
		assertFalse("Incorrect interpret", stantion.getStatus().isRoverError());
	}
	
	
	@Test
	public void interpretMavMsgGpsGlobalOrigin()
	{
		msg_gps_global_origin msg=new msg_gps_global_origin();
		msg.compid=100;		
		msg.sysid=100;
		msg.latitude=(int) (73.126345*1e7);	
		msg.longitude=(int) (52.126345*1e7);
		mint.responseInterpret(msg.pack());
		assertTrue("Incorrect interpret", stantion.getRover().lat==msg.latitude/1e7f);
		assertTrue("Incorrect interpret", stantion.getRover().lon==msg.longitude/1e7f);
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.REFRESH_IM_VIEW);
	}
	
	@Test
	public void interpretMavMsgSysStatus()
	{
		msg_sys_status msg=new msg_sys_status();
		msg.compid=100;
		msg.sysid=100;
		msg.onboard_control_sensors_health|=MAV_SYS_STATUS_SENSOR.MAV_SYS_STATUS_SENSOR_GPS;
		msg.battery_remaining=82;		
		stantion.getRover().setGps_is_alive(false);
		mint.responseInterpret(msg.pack());
		assertTrue("Incorrect interpret", stantion.getRover().getBattery_remaining()==msg.battery_remaining);
		assertTrue("Incorrect interpret", stantion.getRover().gpsIsAlive());
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.REFRESH_BATTERY_STATUS);
		msg.onboard_control_sensors_health &=~MAV_SYS_STATUS_SENSOR.MAV_SYS_STATUS_SENSOR_GPS;
		String text="���������� ������� GPS-��������� ������ 6. �������������� ������������ ����������.";
		mint.responseInterpret(msg.pack());
		assertFalse("Incorrect interpret", stantion.getRover().gpsIsAlive());
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.SET_ERROR_TEXT);
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getParameter(), text);
	}
	
	@Test
	public void interpretMavMsgSystemTime()
	{
		msg_system_time msg=new msg_system_time();
		msg.compid=100;		
		msg.sysid=100;
		msg.time_boot_ms=1234567;
		mint.responseInterpret(msg.pack());
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getAction(), RunActionEnum.SET_ROVER_WORK_TIME);
		assertEquals("Incorrect GUI action", stantion.getFXRunner().getParameter(), msg.time_boot_ms);
	}

	@Test
	public void sendSetModeCommand()
	{
		try{
			mint.sendSetModeCommand();
			fail("No generate Exception");
		} catch (NoOpenConnectionException ex)
		{
			mint.openConnection("Test Port");
			try
			{		
				for(int i=0;i<ControlMode.values().length;i++)
				{
					stantion.getStatus().setControlMode(ControlMode.values()[i]);
					mint.sendSetModeCommand();
					MAVLinkPacket data=(MAVLinkPacket)connector.getLastCallMethodData();
					assertEquals("Incorrect msg id", data.msgid, msg_set_mode.MAVLINK_MSG_ID_SET_MODE);
					msg_set_mode msg=new msg_set_mode();
					msg.unpack(data.payload);
					int mav_mode=Utils.ConvertControlModeEnumToMAV_MODE(ControlMode.values()[i]);
					assertEquals("Incorrect payload", msg.base_mode, mav_mode);
				}
			} catch (NoOpenConnectionException ex1)
			{
				fail();
			}
		}
	}
	
	@Test
	public void sendManualControlCommand()
	{
		try{
			mint.sendManualControlCommand((short)0, (short)0, (short)0);
			fail("No generate Exception");
		} catch (NoOpenConnectionException ex)
		{
			mint.openConnection("Test Port");
			try
			{		
				for(short speed=-1000, angle=-180;speed<=1000;speed+=100, angle+=10)
				{
					mint.sendManualControlCommand(speed, (short)-speed, angle);
					MAVLinkPacket data=(MAVLinkPacket)connector.getLastCallMethodData();
					assertEquals("Incorrect msg id", data.msgid, msg_manual_control.MAVLINK_MSG_ID_MANUAL_CONTROL);
					msg_manual_control msg=new msg_manual_control();
					msg.unpack(data.payload);
					assertEquals("Incorrect payload", msg.x, speed);
					assertEquals("Incorrect payload", msg.y, (short)-speed);
					assertEquals("Incorrect payload", msg.z, angle);					
				}
			} catch (NoOpenConnectionException ex1)
			{
				fail();
			}
		}
	}
	
	@Test
	public void sendWaypointCount()
	{
		try{
			mint.sendWaypointCount(0);
			fail("No generate Exception");
		} catch (NoOpenConnectionException ex)
		{
			mint.openConnection("Test Port");
			try
			{		
					int waypoint_count=111;
					mint.sendWaypointCount(waypoint_count);
					MAVLinkPacket data=(MAVLinkPacket)connector.getLastCallMethodData();
					assertEquals("Incorrect msg id", data.msgid, msg_mission_count.MAVLINK_MSG_ID_MISSION_COUNT);
					msg_mission_count msg=new msg_mission_count();
					msg.unpack(data.payload);
					assertEquals("Incorrect payload", msg.count, waypoint_count);				
			} catch (NoOpenConnectionException ex1)
			{
				fail();
			}
		}
	}
	
	@Test
	public void sendWaypointItem()
	{
		MapMark m=new MapMark(10.1f, 11.1f, false, 0);
		m.setSpeed(1.3f);
		try{			
			mint.sendWaypointItem(m, null, 0);
			fail("No generate Exception");
		} catch (NoOpenConnectionException ex)
		{
			mint.openConnection("Test Port");
			try
			{		
				int waypoint_id=10;	
				
				mint.sendWaypointItem(m, null, waypoint_id);
				MAVLinkPacket data=(MAVLinkPacket)connector.getLastCallMethodData();
				assertEquals("Incorrect msg id", data.msgid, msg_mission_item.MAVLINK_MSG_ID_MISSION_ITEM);
				msg_mission_item msg=new msg_mission_item();
				msg.unpack(data.payload);
				//x - lat
				//y - lon
				assertTrue("Incorrect payload", msg.x==m.lat);
				assertTrue("Incorrect payload", msg.y==m.lon);
				//param1 obj lat if obj is null => -1000
				//param2 obj lon if obj is null => -1000
				//param3 v
				assertTrue("Incorrect payload", msg.param1==-1000);
				assertTrue("Incorrect payload", msg.param2==-1000);
				assertTrue("Incorrect payload", msg.param3==m.getSpeed());
				assertTrue("Incorrect payload", msg.seq==waypoint_id);
				
				MapMark obj=new MapMark(-10f, -2f, true, -1);
				mint.sendWaypointItem(m, obj, waypoint_id);
				data=(MAVLinkPacket)connector.getLastCallMethodData();
				assertEquals("Incorrect msg id", data.msgid, msg_mission_item.MAVLINK_MSG_ID_MISSION_ITEM);
				msg=new msg_mission_item();
				msg.unpack(data.payload);
				//x - lat
				//y - lon
				assertTrue("Incorrect payload", msg.x==m.lat);
				assertTrue("Incorrect payload", msg.y==m.lon);
				//param1 obj lat if obj is null => -1000
				//param2 obj lon if obj is null => -1000
				//param3 v
				assertTrue("Incorrect payload", msg.param1==obj.lat);
				assertTrue("Incorrect payload", msg.param2==obj.lon);
				assertTrue("Incorrect payload", msg.param3==m.getSpeed());
				assertTrue("Incorrect payload", msg.seq==waypoint_id);
					
			} catch (NoOpenConnectionException ex1)
			{
				fail();
			}	
		}
		try{
			mint.sendWaypointItem(m, null, -1);
			fail("No generate Exception");
		}
		catch(InvalidParameterException ex)	{	} 
		catch (NoOpenConnectionException e) {
			fail("Generate invalid exception");
		}
		
		try{
			mint.sendWaypointItem(null, null, 0);
			fail("No generate Exception");
		}
		catch(InvalidParameterException ex)	{	} 
		catch (NoOpenConnectionException e) {
			fail("Generate invalid exception");
		}
	}
	
}
