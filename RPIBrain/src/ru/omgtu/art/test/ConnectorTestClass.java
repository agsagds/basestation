package ru.omgtu.art.test;

import java.util.logging.Logger;

import com.MAVLink.MAVLinkPacket;

import ru.omgtu.art.projectbot.communication.IConnector;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.communication.UARTRadio;
import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;

public class ConnectorTestClass implements IConnector {
	private boolean connect_status=false;
	private final Logger log = Logger.getLogger("UART Radio Test logger");
	@Override
	public void sendMsg(String msg) throws NoOpenConnectionException
    {
		log.info("sending msg: "+msg);
    }
	@Override
	public void disconnect() 
	{
		connect_status=false;
	    log.info("UART Radio Test disconnected");
    }
	@Override
	public void sendMsg(int msg[])  throws NoOpenConnectionException
    {
		 log.info("Sending byte[] msg: "+msg);
    }
	@Override
	public void sendMsg(MAVLinkPacket msg)  throws NoOpenConnectionException
    {
		log.info("Sending MAVLinkPacket: "+msg.unpack().toString());
    }
	@Override
	public void connect(String port_name, int baud_rate, boolean mavlink_protocol, MsgInterpeter mint) {
		connect_status=true;
		log.info("TestUARTRAdio connected");		
	}
	@Override
	public boolean isConnected() {
		return connect_status;
	}
}
