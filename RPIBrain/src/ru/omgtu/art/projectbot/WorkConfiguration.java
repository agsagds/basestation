package ru.omgtu.art.projectbot;

import org.apache.commons.lang3.NotImplementedException;

public class WorkConfiguration {
	private double radius=30; //��������� (� ��������) �� view_mark, �� ������� ��� ��������� ��������� � ������ set_connect
	private double max_path_length=900; //����������� ���������� ��������� ������� (� ������) 
	private int max_work_time=60*60*3; //����������� ���������� ����� ������ ������ (� ��������)
	private int heart_beat_period_seconds=1;//������ ����� ����� ���������� �� ������ (� ��������)
	
	public void loadConfig()
	{
		//load config here
	}

	public double getRadius() {
		return radius;
	}

	public double getMaxPathLength() {
		return max_path_length;
	}

	public int getMaxWorkTime() {
		return max_work_time;
	}

	public int getHeartBeatPeriodSeconds() {
		return heart_beat_period_seconds;
	}

	
	
}
