package ru.omgtu.art.projectbot;

import ru.omgtu.art.mapapi.Rover;
import ru.omgtu.art.projectbot.communication.IConnector;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.graphic.IFXController;
import ru.omgtu.art.projectbot.graphic.RunActionEnum;

public interface IBaseStantion {
	public SystemStatus getStatus();
	public MsgInterpeter getMsgInterpreter();
	public IFXController getFXController();
	public Rover getRover();
	public MissionData getMissionData();
	public WorkConfiguration getWorkConfig();
	public void init(IConnector connector, IFXController fxcontroller);
	public void runGUIAction(RunActionEnum action, Object parameter);
}
