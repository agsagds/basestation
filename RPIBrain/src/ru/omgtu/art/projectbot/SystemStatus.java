package ru.omgtu.art.projectbot;

public class SystemStatus {
	private boolean response_recived=false;	
	private boolean mav_err=false;
	private boolean rover_error=false;
	private int waypoint_id=-1;
	private long heartbeat_last_time=0;
	private ControlMode control_mode=ControlMode.AUTO_DISARMED;
	
	public boolean isResponseRecived() {
		return response_recived;
	}
	public void setRecivedResponseStatus() {
		this.response_recived = true;
	}
	public void clrRecivedResponseStatus() {
		this.response_recived = false;
	}
	
	public boolean isMavErr() {
		return mav_err;
	}
	public void setMavErrStatus() {
		this.mav_err = true;
	}
	public void clrMavErrStatus() {
		this.mav_err = false;
	}
	
	public boolean isRoverError() {
		return rover_error;
	}
	public void setRoverErrorStatus() {
		this.rover_error = true;
	}
	public void clrRoverErrorStatus() {
		this.rover_error = false;
	}
	
	public int getNextWaypointId() {
		return waypoint_id;
	}
	public void setNextWaypointId(int waypoint_id) {
		this.waypoint_id = waypoint_id;
	}
	
	public long getHeartbeatRecivedLastTime() {
		return heartbeat_last_time;
	}
	public void setHeartbeatRecivedLastTimeNow() {
		this.heartbeat_last_time = System.currentTimeMillis();
	}
	
	public ControlMode getControlMode() {
		return control_mode;
	}
	public void setControlMode(ControlMode control_mode) {
		this.control_mode = control_mode;
	}
}
