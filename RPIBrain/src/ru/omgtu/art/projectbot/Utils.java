package ru.omgtu.art.projectbot;

import java.util.List;

import com.MAVLink.enums.MAV_MODE;

import ru.omgtu.art.mapapi.MapMark;
import ru.omgtu.art.mapapi.PathMark;

public class Utils {
	public static double calcMarksPathLength(List<PathMark> path_marks)
	{
		double r=0;
		double cos_gam;
		final double earth_middle_radius=6371000; //in meters
		//formula source: http://www.aleprojects.com/ru/doc/simplegeo
		for(int i=0;i<path_marks.size()-1;i++)
		{
			cos_gam=Math.cos(Math.toRadians(path_marks.get(i).lat))*Math.cos(Math.toRadians(path_marks.get(i+1).lat));
			cos_gam*=(Math.cos(Math.toRadians(path_marks.get(i).lon-path_marks.get(i+1).lon))-1);
			cos_gam+=Math.cos(Math.toRadians(path_marks.get(i).lat-path_marks.get(i+1).lat));
			r+=earth_middle_radius*Math.acos(cos_gam);
		}
		return r;		
	}
	
	public static int ConvertControlModeEnumToMAV_MODE(ControlMode c_mode)
	{
		int m_mode;
		switch(c_mode)
		{
		case AUTO_DISARMED:
			m_mode=MAV_MODE.MAV_MODE_AUTO_DISARMED;
			break;
		case AUTO_ARMED:
			m_mode=MAV_MODE.MAV_MODE_AUTO_ARMED;
			break;
		case MANUAL_DISARMED:
			m_mode=MAV_MODE.MAV_MODE_MANUAL_DISARMED;
			break;
		case MANUAL_ARMED:
			m_mode=MAV_MODE.MAV_MODE_MANUAL_ARMED;
			break;
		default:
			m_mode=-1;
			break;
		}
		return m_mode;
	}
}
