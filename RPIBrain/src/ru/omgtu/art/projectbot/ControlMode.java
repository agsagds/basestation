package ru.omgtu.art.projectbot;

public enum ControlMode {
	AUTO_DISARMED, //автоматический режим
	AUTO_ARMED,
	MANUAL_DISARMED, //ручной режим управления
	MANUAL_ARMED
}
