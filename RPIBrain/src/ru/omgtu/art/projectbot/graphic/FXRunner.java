package ru.omgtu.art.projectbot.graphic;

public class FXRunner implements Runnable {

	private RunActionEnum action;
	private IFXController controller;
	private Object param;
	
	public FXRunner(RunActionEnum action, IFXController controller, Object param)
	{
		this.action=action;
		this.controller=controller;
		this.param=param;
	}
	
	@Override
	public void run() {
		switch(this.action)
		{
		case REFRESH_IM_VIEW:
			controller.refreshImView();
			break;
		case SET_ACTIVE_PATH_MARK:
			controller.setActivePathMark((int)param);
			break;
		case SET_ERROR_TEXT:
			controller.setLabelText((String)param, true);
			break;
		case REFRESH_BATTERY_STATUS:
			controller.refreshBatteryPb();
			break;
		case SET_ROVER_WORK_TIME:
			controller.setRoverWorkTime((long)param);
			break;
		case REFRESH_HB_INDICATOR:
			controller.refreshHeartBeatIndicator();
			break;
		case SHOW_ERROR_ALERT:
			controller.showErrorAlert((String)param);
			break;
		case SHOW_WARNING_ALERT:
			controller.showWarningAlert((String)param);
			break;
		case SHOW_INFO_ALERT:
			controller.showInfoAlert((String)param);
			break;
		default:
			break;
		}
	}
	
	public RunActionEnum getAction()
	{
		return action;
	}
	
	public Object getParameter()
	{
		return param;
	}

}
