package ru.omgtu.art.projectbot.graphic;

import java.time.LocalTime;

import javafx.application.Platform;
import ru.omgtu.art.projectbot.GlobalVariables;

public class HeartBeatRefresher extends Thread{
	 
	private int period_mills;
	private volatile boolean active;
	private FXRunner runner;
	
	public HeartBeatRefresher(int period_mills, IFXController fxcontroller)
	   {
	      this.period_mills=period_mills;
	      runner=new FXRunner(RunActionEnum.REFRESH_HB_INDICATOR, fxcontroller, null);
	   }

	   @Override
	   public void run()
	   {
		   this.active=true;
		   while(active)
		   {
			   Platform.runLater(runner);
		     try {
				Thread.sleep(period_mills);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		   }
	   }
	   
	   public void deactivate()
	   {
		   this.active=false;
	   }
}