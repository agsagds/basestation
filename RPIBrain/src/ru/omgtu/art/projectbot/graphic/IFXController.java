package ru.omgtu.art.projectbot.graphic;

import javafx.scene.control.Alert;
import ru.omgtu.art.projectbot.IBaseStantion;

public interface IFXController {
	public void init(IBaseStantion base_stantion);
	
	public void dispose();
	
	public void refreshHeartBeatIndicator();
	
	public void refreshImView();
	
	public void refreshBatteryPb();
	
	public void setLabelText(String text, boolean is_err);
	
	public void setRoverWorkTime(long mills);
	
	public void setActivePathMark(int path_mark_number);
	
	public void showErrorAlert(String message);
	
	public void showWarningAlert(String message);
	
	public void showInfoAlert(String message);
}
