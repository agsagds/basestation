package ru.omgtu.art.projectbot.graphic;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.time.LocalTime;
import java.util.concurrent.TimeoutException;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import ru.omgtu.art.mapapi.Drawer;
import ru.omgtu.art.mapapi.Frame;
import ru.omgtu.art.mapapi.MapAPI;
import ru.omgtu.art.mapapi.MapMark;
import ru.omgtu.art.mapapi.MarkState;
import ru.omgtu.art.mapapi.PathMark;
import ru.omgtu.art.projectbot.ControlMode;
import ru.omgtu.art.projectbot.IBaseStantion;
import ru.omgtu.art.projectbot.Utils;
import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;

public class FXController implements IFXController {
	private IBaseStantion base_stantion;
	private HeartBeatRefresher hb_refresher;	
	private volatile Frame frame;	
	private volatile MapMark selected_path_mark;
	private volatile int selected_obj_mark=-1;
	private boolean wasd[];	
	@FXML
	private AnchorPane auto_pane;
	@FXML
	private AnchorPane manual_pane;
	@FXML
	private ImageView im_view;
	@FXML
	private TableView<MapMark> tbv_marks;
	@FXML
	private ChoiceBox<String> chb_mark_value;
	@FXML
	private ChoiceBox<String> chb_obj_type;
	@FXML
	private Button bt_connect;
	@FXML
	private Button bt_load;
	@FXML
	private Button bt_remove;
	@FXML
	private Button bt_send;
	@FXML
	private Button bt_left;
	@FXML
	private Button bt_right;
	@FXML
	private Button bt_up;
	@FXML
	private Button bt_down;
	@FXML
	private Button bt_set_value;
	@FXML
	private Button bt_auto_mode;
	@FXML
	private Button bt_manual_mode;
	@FXML
	private CheckBox cb_active;
	@FXML
	private CheckBox cb_return_path_repeat;
	@FXML
	private TextField tb_connector_port;
	@FXML
	private TextField tb_long;
	@FXML
	private TextField tb_lat;
	@FXML
	private TextField tb_value;
	@FXML
	private Slider sl_zoom;
	@FXML
	private Slider sl_speed;
	@FXML
	private Slider sl_head;
	@FXML
	private Label lb_info;
	@FXML
	private Label lb_error;
	@FXML
	private Label lb_time;
	@FXML
	private ProgressBar pb_battery;
	@FXML
	private ProgressIndicator pi_hb;
	@Override
	public void init(IBaseStantion base_stantion) {	
		//set mint
		this.base_stantion=base_stantion;		
		//init frame
		frame=new Frame(480, 480, "frame.png");
		frame.setLat(55.026472f);
        frame.setLon(73.291216f);
        frame.setZoom(15);      	
        
		//init TableView
		configureTableView();		
		//init choise box mark value items
		ObservableList<String> choises=FXCollections.observableArrayList();
		choises.add("��������");
		choises.add("�������");
		choises.add("������");
		chb_mark_value.setItems(choises);
		chb_mark_value.setValue(chb_mark_value.getItems().get(0));
		//init choise box object type items
		choises=FXCollections.observableArrayList();
		choises.add("�������� �������");
		choises.add("����� ����");
		choises.add("������ ��������");
		choises.add("�����");
		chb_obj_type.setItems(choises);
		chb_obj_type.setValue(chb_obj_type.getItems().get(0));
		//set panes state
		refreshPanesState();
	}
	
	@Override
	public void dispose()
	{		
		if(hb_refresher!=null)
			hb_refresher.deactivate();
	}
	
	@FXML
	protected void cb_active_state_changed(ActionEvent event) throws IOException {
		ControlMode old_mode=base_stantion.getStatus().getControlMode();
		try{			
			if(cb_active.isSelected() && base_stantion.getStatus().getControlMode()==ControlMode.AUTO_DISARMED)
			{
				base_stantion.getStatus().setControlMode(ControlMode.AUTO_ARMED);
				base_stantion.getMsgInterpreter().sendSetModeCommand();
			}
			else if(cb_active.isSelected() && base_stantion.getStatus().getControlMode()==ControlMode.MANUAL_DISARMED)
			{
				base_stantion.getStatus().setControlMode(ControlMode.MANUAL_ARMED);
				base_stantion.getMsgInterpreter().sendSetModeCommand();
				manual_pane.requestFocus();
			}
			else if(!cb_active.isSelected() && base_stantion.getStatus().getControlMode()==ControlMode.MANUAL_ARMED)
			{
				base_stantion.getStatus().setControlMode(ControlMode.MANUAL_DISARMED);
				base_stantion.getMsgInterpreter().sendSetModeCommand();
			}
			else if(!cb_active.isSelected() && base_stantion.getStatus().getControlMode()==ControlMode.AUTO_ARMED)
			{
				base_stantion.getStatus().setControlMode(ControlMode.AUTO_DISARMED);
				base_stantion.getMsgInterpreter().sendSetModeCommand();
			}
		} catch(NoOpenConnectionException ex)
		{
			cb_active.setSelected(!cb_active.isSelected());
			base_stantion.getStatus().setControlMode(old_mode);
			String msg="���������� ������������ ������. ��������� ����������� ����������";
			showErrorAlert(msg);
			setLabelText(msg, true);
		}
		
	}
	
	@FXML
	protected void sl_zoom_scroll_finished(MouseEvent event) throws IOException {
		frame.setZoom((int)sl_zoom.getValue());
	    frameRefresh();    
	}
	
	@FXML
	protected void bt_connect_click(ActionEvent event) throws IOException {
		//connect UART
		if(base_stantion.getMsgInterpreter().openConnection(tb_connector_port.getText()))
		{			
			hb_refresher=new HeartBeatRefresher(100, this);			
			//heartbeat refresher start;
			hb_refresher.start();
			//hide button
			bt_connect.setVisible(false);
			//show heartbeat indicator
			pi_hb.setVisible(true);
		} else
		{
			String alert_msg="����������� �� �������. ������ �������� COM-����";
			showAlert(AlertType.ERROR, alert_msg);			
		}
	}
	
	@FXML
	protected void bt_mode_click(ActionEvent event) throws IOException {
		Button s=(Button)event.getSource();
		String msg="";
		switch(s.getId())
		{
		case "bt_manual_mode":
			base_stantion.getStatus().setControlMode(ControlMode.MANUAL_DISARMED);
			msg="������ ����� ���������� �������";
			setLabelText(msg, false);
			showInfoAlert(msg);
			break;
		case "bt_auto_mode":
			base_stantion.getStatus().setControlMode(ControlMode.AUTO_DISARMED);
			msg="������ ����� ���������� ��������";
			setLabelText(msg, false);
			showInfoAlert(msg);
			break;
		default:
			break;
		}
		try {
			base_stantion.getMsgInterpreter().sendSetModeCommand();
		} catch (NoOpenConnectionException e) {
			msg="�� ������� ��������� ���������. ��������� ����������� ����������";
			showWarningAlert(msg);
			setLabelText(msg, true);
		}
		refreshPanesState();
	}
	
	@FXML
	protected void bt_load_click(ActionEvent event) throws IOException {
		frame.setZoom((int)sl_zoom.getValue());
		try{
	        frame.setLat(Float.valueOf(tb_lat.getText()));
	        frame.setLon(Float.valueOf(tb_long.getText()));
	        frameRefresh();
		} catch (NumberFormatException e) {
			showErrorAlert("������! �������� ������ ������");
		}        
	}
	
	@FXML
	protected void bt_navigation_click(ActionEvent event) throws IOException {
		
		Button s=(Button)event.getSource();
		if(base_stantion.getStatus().getControlMode()==ControlMode.AUTO_ARMED || base_stantion.getStatus().getControlMode()==ControlMode.AUTO_DISARMED) 
		{
			switch(s.getId())
			{
			case "bt_up":
				frame.setLat(frame.getLat() + MapAPI.yToLat(120, frame.getZoom()));
				break;
			case "bt_down":
				frame.setLat(frame.getLat() - MapAPI.yToLat(120, frame.getZoom()));
				break;
			case "bt_left":
				frame.setLon(frame.getLon() - MapAPI.xToLon(120, frame.getZoom()));
				break;
			case "bt_right":
				frame.setLon(frame.getLon() + MapAPI.xToLon(120, frame.getZoom()));
				break;
			}
			frameRefresh();
		} else if (base_stantion.getStatus().getControlMode()==ControlMode.MANUAL_ARMED) {
			switch(s.getId())
			{
			case "bt_up":
				wasd[0]=true;
				moveCommand();
				break;
			case "bt_down":
				wasd[2]=true;
				moveCommand();
				break;
			case "bt_left":
				wasd[1]=true;
				moveCommand();
				break;
			case "bt_right":
				wasd[3]=true;
				moveCommand();
				break;
			default:
				moveCommand();
				break;
			}
			manual_pane.requestFocus();
		}
	}
	
	@FXML
	protected void bt_remove_click(ActionEvent event) throws IOException {	
		int mark_id=tbv_marks.getSelectionModel().getSelectedIndex()-1;
		switch((String)chb_obj_type.getValue())
		{
		case "����� ����":			
			if(mark_id<0 || mark_id>base_stantion.getMissionData().getPathMarks().size())
			{
				if(!base_stantion.getMissionData().getPathMarks().isEmpty())
				{
					base_stantion.getMissionData().getPathMarks().remove(base_stantion.getMissionData().getPathMarks().size()-1);
		            setLabelText("��������� ����� ���� �������. ���������� ���������� �����: "+base_stantion.getMissionData().getPathMarks().size(), false);
				}
			} else {
				base_stantion.getMissionData().getPathMarks().remove(mark_id);
	            setLabelText("����� ���� "+mark_id+" �������. ���������� ���������� �����: "+base_stantion.getMissionData().getPathMarks().size(), false);
			}
			break;
		case "������ ��������":
			if(!base_stantion.getMissionData().getObjMarks().isEmpty())
			{
				base_stantion.getMissionData().getObjMarks().remove(base_stantion.getMissionData().getObjMarks().size() - 1);
	            for(MapMark m : base_stantion.getMissionData().getPathMarks())
	            	if(m.getObj()==base_stantion.getMissionData().getObjMarks().size())
	            		m.setObj(-1);
	            setLabelText("��������� ������ �������� ������. ���������� ���������� �����: "+base_stantion.getMissionData().getObjMarks().size(), false);
			}
			break;
		case "�����":
			if(mark_id<0 || mark_id>base_stantion.getMissionData().getPathMarks().size())
			{
				if(!base_stantion.getMissionData().getPathMarks().isEmpty())
				{
					if(selected_path_mark!=null)
		        	{
		        		selected_path_mark.setState(MarkState.DEFAULT);
		        		selected_path_mark.setObj(-1);
		        		selected_path_mark=null;
		        	}    
				}		
			} else {
				base_stantion.getMissionData().getPathMarks().get(mark_id).setState(MarkState.DEFAULT);
				base_stantion.getMissionData().getPathMarks().get(mark_id).setObj(-1);
			}
			break;
		default:
			break;
		}
		refreshImView();
		refreshTableView();
	}
	
	@FXML
	protected void bt_send_click(ActionEvent event) throws IOException {
		String msg="";
		try {
			//sending data through mavlink here
			do {
				base_stantion.getStatus().clrRecivedResponseStatus();
				try {
					base_stantion.getMsgInterpreter().sendWaypointCount(base_stantion.getMissionData().getPathMarks().size()+1);
				} catch (NoOpenConnectionException e) {
					msg="�� ������� ��������� ���������. ��������� ����������� ����������";
					showErrorAlert(msg);
					setLabelText(msg, true);
					return;
				}
				msg="���������� ����� ���� ����������. �������� ������";
				setLabelText(msg, false);
				waitResponse(1);
				msg="����� �������. ����������� ������";
				setLabelText(msg, false);
				//sended marks
				while(base_stantion.getStatus().getNextWaypointId()>=0 && base_stantion.getStatus().getNextWaypointId()<=base_stantion.getMissionData().getPathMarks().size())
				{
					MapMark m=null;
					//send home mark
					if(base_stantion.getStatus().getNextWaypointId()==0)
						m=base_stantion.getMissionData().getHomeMark();
					else
						m=base_stantion.getMissionData().getPathMarks().get(base_stantion.getStatus().getNextWaypointId()-1);
					//sended marks
					MapMark obj=(m.getObj()>=0 ? base_stantion.getMissionData().getObjMarks().get(m.getObj()) : null);
					base_stantion.getStatus().clrRecivedResponseStatus();
					try {
						base_stantion.getMsgInterpreter().sendWaypointItem(m, obj, base_stantion.getStatus().getNextWaypointId());
					} catch (InvalidParameterException e) {
						msg="����� �������� ������������� �������������� ����� ����";
						showErrorAlert(msg);
						return;
					} catch (NoOpenConnectionException e) {
						msg="�� ������� ��������� ���������. ��������� ����������� ����������";
						showErrorAlert(msg);
						setLabelText(msg, true);
						return;
					}
					waitResponse(1);
				}
			} while(base_stantion.getStatus().isMavErr());
			msg="�������� ������ ����� ������� ���������";
			showInfoAlert(msg);
			setLabelText(msg, false);
		} catch (TimeoutException exp)
		{
			msg="������! "+exp.getMessage();
			showErrorAlert(msg);
			setLabelText(msg, true);
			return;
		}
	}
	
	@FXML
	protected void bt_set_value_click(ActionEvent event) throws IOException {
		int mark_id=tbv_marks.getSelectionModel().getSelectedIndex();
		if(mark_id<0 || mark_id>base_stantion.getMissionData().getPathMarks().size())
			return;
		float value=0;
		try
		{
			value=Float.valueOf(tb_value.getText());
		} catch (NumberFormatException ex) {
			String msg="���� �������� ������ ������������� �������";
			showErrorAlert(msg);
			setLabelText(msg, true);
			return;
		}
		mark_id--;
		switch((String)chb_mark_value.getValue())
		{
		case "��������":
			if(mark_id<0)
				base_stantion.getMissionData().getHomeMark().setSpeed(value);
			else
				base_stantion.getMissionData().getPathMarks().get(mark_id).setSpeed(value);
			break;
		case "�������" :
			if(mark_id<0)
				base_stantion.getMissionData().getHomeMark().lon=value;
			else
				base_stantion.getMissionData().getPathMarks().get(mark_id).lon=value;
			break;
		case "������" :
			if(mark_id<0)
				base_stantion.getMissionData().getHomeMark().lat=value;
			else
				base_stantion.getMissionData().getPathMarks().get(mark_id).lat=value;
			break;
		default:
			System.out.println("Oops! No selection");
			break;		
		}
		refreshImView();
		refreshTableView();
	}
	
	@FXML
	protected void im_view_click(MouseEvent event) throws IOException {
		if(event.getButton()!=MouseButton.PRIMARY || !auto_pane.isVisible())
			return;
		String msg="";
		int mid_x = 240, mid_y = 240;
        float mark_lat, mark_lon;
        mark_lat = frame.getLat() + MapAPI.yToLat((int) (mid_y - event.getY()), frame.getZoom());
        mark_lon = frame.getLon() - MapAPI.xToLon((int) (mid_x - event.getX()), frame.getZoom());
      //  System.out.println("MouseX: "+event.getX()+"; MouseY: "+event.getY());
        switch((String)chb_obj_type.getValue())
		{
		case "����� ����":
			if(base_stantion.getMissionData().getHomeMark()==null)
			{
				msg="������� ��������� ���������� �������� �������";
				showErrorAlert(msg);
				setLabelText(msg, true);
			}
			else
			{
				base_stantion.getMissionData().getPathMarks().add(new PathMark(mark_lat, mark_lon, false, base_stantion.getMissionData().getPathMarks().size()));
	        	double path_length=Utils.calcMarksPathLength(base_stantion.getMissionData().getPathMarks());
	        	if(cb_return_path_repeat.isSelected())
	        		path_length*=2;
	        	if(path_length>base_stantion.getWorkConfig().getMaxPathLength())
	        	{
	        		base_stantion.getMissionData().getPathMarks().remove(base_stantion.getMissionData().getPathMarks().size()-1);
	        		msg="��������� ������������ ����� �������� (����������: "+base_stantion.getWorkConfig().getMaxPathLength()+"�; �������: "+(int)path_length+"�)";
	        		setLabelText(msg, true);
	        	}
	        	else
	        	{
	        		refreshTableView();
		        	refreshImView();
		        	msg="����� ����� ���� ���������. ����� ����� ��������: "+(int)path_length+"�";
	        		setLabelText(msg, false);
	        	}	
			}
			break;
		case "������ ��������":
			if(base_stantion.getMissionData().getHomeMark()==null)
			{
				setLabelText("������! ������� ��������� ���������� �������� �������.", true);
			}
			else
			{
				base_stantion.getMissionData().getObjMarks().add(new MapMark(mark_lat, mark_lon, true, base_stantion.getMissionData().getObjMarks().size()));
				msg="����� ������ �������� �������� � �����: ("+mark_lat+", "+mark_lon+")";
	            setLabelText(msg, false);
	            refreshImView();
			}
			break;
		case "�������� �������":
			base_stantion.getMissionData().setHomeMark(new MapMark(mark_lat, mark_lon, false, -1));
			bt_send.setDisable(false);
			refreshTableView();
			refreshImView();
			setLabelText("�������� ������� ����������� � �����: ("+mark_lat+", "+mark_lon+")", false);
			break;
		default:
			break;
		}     
	}
	
	@FXML
	protected void im_view_mouse_pressed(MouseEvent event) throws IOException {
		if(event.getButton()!=MouseButton.PRIMARY || !auto_pane.isVisible())
			return;
        if(((String)chb_obj_type.getValue()).compareTo("�����")==0)
        {
        	if(selected_path_mark==null)
        	{
	        	double min_r=base_stantion.getWorkConfig().getRadius();
	        	for(MapMark m: base_stantion.getMissionData().getPathMarks())
	        	{ 
	        		int x=m.getX(frame.getZoom(), frame.getLon());
	        		int y=m.getY(frame.getZoom(), frame.getLat());
	        		double r=Math.sqrt((x-event.getX())*(x-event.getX())+(y-event.getY())*(y-event.getY()));
	        		if(r<=min_r)
	        		{
	        			min_r=r;
	        			selected_path_mark=m;
	        		}
	        	}
	        	if(selected_path_mark!=null)
	        		selected_path_mark.setState(MarkState.SELECTED);
        	}
        	else
        	{
            	selected_path_mark.setState(MarkState.DEFAULT);
            	if(selected_obj_mark>=0)
            	{
            		base_stantion.getMissionData().getObjMarks().get(selected_obj_mark).setState(MarkState.DEFAULT);
            		selected_path_mark.setObj(selected_obj_mark);
            	}
            	selected_obj_mark=-1;
            	selected_path_mark=null;
        	}
        	refreshImView();
        }        
	}
	
	@FXML
	protected void im_view_mouse_moved(MouseEvent event) throws IOException {
		if(selected_path_mark==null)
			return;
        if(((String)chb_obj_type.getValue()).compareTo("�����")==0)
        {
        	if(selected_obj_mark>=0)
        		base_stantion.getMissionData().getObjMarks().get(selected_obj_mark).setState(MarkState.DEFAULT);
        	selected_obj_mark=-1;
        	double min_r=base_stantion.getWorkConfig().getRadius();
        	int i=0;
        	for(MapMark m: base_stantion.getMissionData().getObjMarks())
        	{ 
        		int x=m.getX(frame.getZoom(), frame.getLon());
        		int y=m.getY(frame.getZoom(), frame.getLat());
        		double r=Math.sqrt((x-event.getX())*(x-event.getX())+(y-event.getY())*(y-event.getY()));
        		if(r<=min_r)
        		{
        			min_r=r;
        			selected_obj_mark=i;        			
        		}
        		i++;
        	}
        	if(selected_obj_mark>=0)
        		base_stantion.getMissionData().getObjMarks().get(selected_obj_mark).setState(MarkState.SELECTED);
        	
        	refreshImView();
        }     
        
	}
	
	@FXML 
	protected void manual_pane_key_pressed(KeyEvent key)
	{	
		switch(key.getCode())
		{
		case W:
			if(!wasd[0])
			{
				wasd[0]=true;
				moveCommand();
			}
			break;
		case A:
			if(!wasd[1])
			{
				wasd[1]=true;
				moveCommand();
			}
			break;
		case S:
			if(!wasd[2])
			{
				wasd[2]=true;
				moveCommand();
			}
			break;
		case D:
			if(!wasd[3])
			{
				wasd[3]=true;
				moveCommand();
			}
			break;
		case ENTER:
			moveCommand();
			break;
		default:
			break;
		}
	}
	
	@FXML 
	protected void manual_pane_key_released(KeyEvent key)
	{
		switch(key.getCode())
		{
		case W:
			wasd[0]=false;
			moveCommand();
			break;
		case A:
			wasd[1]=false;
			moveCommand();
			break;
		case S:
			wasd[2]=false;
			moveCommand();
			break;
		case D:
			wasd[3]=false;
			moveCommand();
			break;
		default:
			break;
		}
	}
	
	@Override
	public void refreshHeartBeatIndicator()
	{
		int hb_sec=base_stantion.getWorkConfig().getHeartBeatPeriodSeconds();
		long delta_t_sec=(System.currentTimeMillis()-base_stantion.getStatus().getHeartbeatRecivedLastTime())/1000;
		double pi_status=0.1+hb_sec-delta_t_sec;
		pi_hb.setProgress(pi_status);
	}
	@Override
	public void refreshImView()
	{
		setImViewImg(Drawer.drawMarks(base_stantion.getMissionData().getHomeMark(), base_stantion.getMissionData().getPathMarks(), base_stantion.getMissionData().getObjMarks(), frame, base_stantion.getRover()));
	}
	@Override
	public void refreshBatteryPb()
	{
		pb_battery.setProgress(base_stantion.getRover().getBattery_remaining());
	}
	@Override
	public void setLabelText(String text, boolean is_err)
	{
		if(is_err)
		{
			lb_info.setVisible(false);
			lb_error.setVisible(true);
			lb_error.setText(text);
		} else
		{
			lb_info.setVisible(true);
			lb_error.setVisible(false);
			lb_info.setText(text);
		}
	}
	
	@Override
	public void showErrorAlert(String message)
	{
		showAlert(AlertType.ERROR, message);
	}
	
	@Override
	public void showWarningAlert(String message)
	{
		showAlert(AlertType.WARNING, message);
	}
	
	@Override
	public void showInfoAlert(String message)
	{
		showAlert(AlertType.INFORMATION, message);
	}
	@Override
	public void setRoverWorkTime(long mills)
	{
		int w_h=(int) ((mills/1000)/3600);
		int w_m=(int) (((mills/1000)%3600)/60);
		int w_s=(int) (((mills/1000)%3600)%60);
		int max_work_time=base_stantion.getWorkConfig().getMaxWorkTime();
		int m_h=max_work_time/3600;
		int m_m=(max_work_time%3600)/60;
		int m_s=max_work_time%60;
		lb_time.setText("����� ������ ������: "+w_h+":"+w_m+":"+w_s+" �� "+m_h+":"+m_m+":"+m_s);
	}
	@Override
	public void setActivePathMark(int path_mark_number)
	{
		path_mark_number--;
		int j=0;
		for (PathMark mark : base_stantion.getMissionData().getPathMarks()) {
			if(j<path_mark_number)
				mark.setAccepted();
			else if(j==path_mark_number)
				mark.setCurrent();
			else
				mark.clrAccepted();				
			j++;
		}
		refreshImView();
	}
	
	private void showAlert(Alert.AlertType type, String message)
	{
		Alert alert = new Alert(type);
		alert.setContentText(message);
		alert.show();
	}
	
	private void moveCommand()
	{
		if(base_stantion.getStatus().getControlMode()!=ControlMode.MANUAL_ARMED)
			return;
		short max_speed=1000;
		short left_speed=0, right_speed=0, head_angle=0;
		head_angle=(short)sl_head.getValue();
		double speed=sl_speed.getValue()/100;
		boolean w,a,s,d;
		w=wasd[0];
		a=wasd[1];
		s=wasd[2];
		d=wasd[3];
		if((w && !a && !s && !d) || (w && a && !s && d))
		{
			left_speed=max_speed;
			right_speed=max_speed;
		} else if ((!w && a && !s && !d) || (w && a && s && !d)) {
			left_speed=(short) -max_speed;
			right_speed=max_speed;
		} else if ((!w && !a && !s && d) || (w && !a && s && d)) {
			left_speed=max_speed;
			right_speed=(short) -max_speed;
		} else if ((!w && !a && s && !d) || (!w && a && s && d)) {
			left_speed=(short) -max_speed;
			right_speed=(short) -max_speed;
		} else if (w && a && !s && !d) {
			left_speed=(short) (max_speed/2);
			right_speed=max_speed;
		} else if (w && !a && !s && d) {
			left_speed=max_speed;
			right_speed=(short) (max_speed/2);
		} else if (!w && a && s && !d) {
			left_speed=(short) (-max_speed/2);
			right_speed=(short) -max_speed;
		} else if (!w && !a && s && d) {
			left_speed=(short) -max_speed;
			right_speed=(short) (-max_speed/2);
		}
		left_speed=(short) (left_speed*speed);
		right_speed=(short) (right_speed*speed);
		try {
			base_stantion.getMsgInterpreter().sendManualControlCommand(left_speed, right_speed, head_angle);
		} catch (NoOpenConnectionException e) {
			String msg="�� ������� ��������� ���������. ��������� ���������� ����������";
			showErrorAlert(msg);
			setLabelText(msg, true);
		}
	}
	
	private void refreshPanesState()
	{		
		if(base_stantion.getStatus().getControlMode()==ControlMode.AUTO_DISARMED || base_stantion.getStatus().getControlMode()==ControlMode.AUTO_ARMED)
		{
			auto_pane.setVisible(true);
			manual_pane.setVisible(false);
			cb_active.setSelected(false);
		} else if (base_stantion.getStatus().getControlMode()==ControlMode.MANUAL_DISARMED || base_stantion.getStatus().getControlMode()==ControlMode.MANUAL_ARMED)
		{
			//reset control values
			wasd=new boolean[4];
			for(int i=0;i<4;i++)
				wasd[i]=false;
			//set panes state
			auto_pane.setVisible(false);
			manual_pane.setVisible(true);
			cb_active.setSelected(false);
		}
	}
	
	private void frameRefresh()
	{
		try {
			frame.setImg((BufferedImage) MapAPI.loadImg(frame));
		} catch (IOException e) {
			showErrorAlert(e.getMessage());
			return;
		}
		setImViewImg(Drawer.drawMarks(base_stantion.getMissionData().getHomeMark(), base_stantion.getMissionData().getPathMarks(), base_stantion.getMissionData().getObjMarks(), frame, base_stantion.getRover()));
		setLabelText("���� ����� ("+frame.getLat()+", "+frame.getLon()+") � ����� x"+frame.getZoom()+" ��������", false);
	}
	
	private synchronized void setImViewImg(BufferedImage img)
	{
		WritableImage wimg=new WritableImage(frame.getW(), frame.getH());
        SwingFXUtils.toFXImage(img, wimg);
        im_view.setImage(wimg);
        //refresh TableView Items
        //refreshTableView();
	}
	
	private void waitResponse(int seconds) throws TimeoutException
	{
		int start_time=LocalTime.now().getSecond();
		while(!base_stantion.getStatus().isResponseRecived())
		{
			if((LocalTime.now().getSecond()-start_time+60)%60>seconds)
				throw new TimeoutException("����� �������� ������ �������");
		}
	}
	
	private synchronized void refreshTableView()
	{
		for(int i=0;i<base_stantion.getMissionData().getPathMarks().size();i++)
			base_stantion.getMissionData().getPathMarks().get(i).index=i;
		 ObservableList<MapMark> marks=FXCollections.observableArrayList(base_stantion.getMissionData().getPathMarks());
		 marks.add(0, base_stantion.getMissionData().getHomeMark());
	     tbv_marks.setItems(marks);
	     tbv_marks.refresh();
	}
	
	private void configureTableView()
	{
		tbv_marks.setTableMenuButtonVisible(true);
		
		//tbv_marks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		TableColumn<MapMark, String> indexCol=new TableColumn<MapMark, String>("Id");
		indexCol.setPrefWidth(30);
		indexCol.setMaxWidth(30);
		indexCol.setMinWidth(30);
		indexCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper<String>(String.valueOf(m.getValue().index));
		     }
		     
		  });
		tbv_marks.getColumns().add(indexCol);
		
		TableColumn<MapMark, String> lonCol=new TableColumn<MapMark, String>("Lon");
		lonCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper<String>(String.valueOf(m.getValue().lon));
		     }
		  });
		tbv_marks.getColumns().add(lonCol);
		
		TableColumn<MapMark, String> latCol=new TableColumn<MapMark, String>("Lat");
		latCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper<String>(String.valueOf(m.getValue().lat));
		     }
		  });
		tbv_marks.getColumns().add(latCol);
		
		TableColumn<MapMark, String> speedCol=new TableColumn<MapMark, String>("V, �/�");
		speedCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper<String>(String.valueOf(m.getValue().getSpeed()));
		     }
		  });
		tbv_marks.getColumns().add(speedCol);
	}
}