package ru.omgtu.art.projectbot.communication;

import java.security.InvalidParameterException;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.common.msg_gps_global_origin;
import com.MAVLink.common.msg_heartbeat;
import com.MAVLink.common.msg_manual_control;
import com.MAVLink.common.msg_mission_ack;
import com.MAVLink.common.msg_mission_count;
import com.MAVLink.common.msg_mission_current;
import com.MAVLink.common.msg_mission_item;
import com.MAVLink.common.msg_mission_request;
import com.MAVLink.common.msg_scaled_imu;
import com.MAVLink.common.msg_set_mode;
import com.MAVLink.common.msg_sys_status;
import com.MAVLink.common.msg_system_time;
import com.MAVLink.enums.MAV_AUTOPILOT;
import com.MAVLink.enums.MAV_MISSION_RESULT;
import com.MAVLink.enums.MAV_MODE;
import com.MAVLink.enums.MAV_MODE_FLAG;
import com.MAVLink.enums.MAV_STATE;
import com.MAVLink.enums.MAV_SYS_STATUS_SENSOR;
import com.MAVLink.enums.MAV_TYPE;

import ru.omgtu.art.mapapi.MapMark;
import ru.omgtu.art.projectbot.IBaseStantion;
import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;
import ru.omgtu.art.projectbot.graphic.RunActionEnum;
import ru.omgtu.art.test.ConnectorTestClass;

public class MsgInterpeter  {
	
	private int sys_id;//system id (what system is sending this message)
	private int component_id;//component id (what component of the system is sending the message)
	private short target_id;
	private short target_component;
	private int sequence;
	private short system_status;
	private short base_mode;
	private short type=MAV_TYPE.MAV_TYPE_GCS;
	private short autopilot=MAV_AUTOPILOT.MAV_AUTOPILOT_GENERIC_WAYPOINTS_AND_SIMPLE_NAVIGATION_ONLY;
	private IConnector connector;
	private IBaseStantion base_stantion;
	
	public MsgInterpeter(int _sys_id, int _component_id, int _sequence_start, short _target_id, short _target_component, IBaseStantion base_stantion, IConnector connector)
	{
		this.sys_id=_sys_id;
		this.component_id=_component_id;
		this.sequence=_sequence_start;
		this.system_status=MAV_STATE.MAV_STATE_STANDBY;
		this.base_mode=MAV_MODE_FLAG.MAV_MODE_FLAG_AUTO_ENABLED;
		this.target_component=_target_component;
		this.target_id=_target_id;
		this.connector=connector;
		this.base_stantion=base_stantion;
	}
	
	public IConnector getConnector()
	{
		return this.connector;
	}
	
	public boolean openConnection(String port_name)
	{
		try {
			
			connector.connect(port_name, 57600, true, this);
			return true;
		} catch (Exception e) {
			System.out.println("Exception: ������ ������������� Serial-�����");
			//connector=new ConnectorTestClass();
			//System.out.println("Serial-���� ������� � ������ �����");
			return false;
		}
	}
	
	public void closeConnection()
	{
		this.connector.disconnect();
	}
	
	public void generateHeartBeat() throws NoOpenConnectionException
	{
		MAVLinkPacket hbpack;
		msg_heartbeat hb = new msg_heartbeat();
		hb.autopilot=this.autopilot;
        hb.base_mode = this.base_mode;
        hb.type=this.type;
        hb.system_status = this.system_status;
        hb.mavlink_version=3;
        hbpack=hb.pack();
        hbpack.seq=this.sequence;
        connector.sendMsg(hbpack);
        incSeq();
	}
	
	/*public void generateResponse(MAVLinkPacket req_packet) throws NoOpenConnectionException
	{
		MAVLinkPacket resp_pack=null;
		
		switch(req_packet.msgid)
		{
		case msg_heartbeat.MAVLINK_MSG_ID_HEARTBEAT:
			generateHeartBeat();
			break;
		
		case msg_request_data_stream.MAVLINK_MSG_ID_REQUEST_DATA_STREAM:
        
        	msg_request_data_stream req_msg=(msg_request_data_stream) req_packet.unpack();
        	msg_data_stream resp_msg=new msg_data_stream();
        	resp_msg.stream_id=req_msg.req_stream_id;
        	resp_msg.message_rate=req_msg.req_message_rate;        	
        	resp_msg.on_off=(short) (req_msg.req_stream_id==6 ? 1 : 0);
        	
        	resp_pack=resp_msg.pack();
        	
        	resp_pack.seq=this.sequence;
        	incSeq();
        	break;
		
		case msg_param_request_list.MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
			//sending param list (throuth param_value)
			break;
		case msg_param_request_read.MAVLINK_MSG_ID_PARAM_REQUEST_READ:
			byte param_id[]=((msg_param_request_read)(req_packet.unpack())).param_id;
			String str="";
			for(int i=0;i<param_id.length;i++)
				str+=param_id[i];
			System.out.println("PARAM_ID!!! "+str);
			
			break;
        	
        default : 
        	break;
		}
        connector.sendMsg(resp_pack);
	}*/
	
	public void responseInterpret(MAVLinkPacket res_packet)
	{
		switch(res_packet.msgid)
		{
		case msg_mission_request.MAVLINK_MSG_ID_MISSION_REQUEST:
			msg_mission_request mission_request=new msg_mission_request();
			mission_request.unpack(res_packet.payload);
			base_stantion.getStatus().setNextWaypointId(mission_request.seq);
			base_stantion.getStatus().setRecivedResponseStatus();
			break;
		case msg_mission_ack.MAVLINK_MSG_ID_MISSION_ACK:
			msg_mission_ack ack=new msg_mission_ack();
			ack.unpack(res_packet.payload);
			base_stantion.getStatus().setNextWaypointId(-1);
			base_stantion.getStatus().clrMavErrStatus();
			if(ack.type!=MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED)
				base_stantion.getStatus().setMavErrStatus();				
			base_stantion.getStatus().setRecivedResponseStatus();
			break;
		case msg_gps_global_origin.MAVLINK_MSG_ID_GPS_GLOBAL_ORIGIN:
			msg_gps_global_origin gps=new msg_gps_global_origin();
			gps.unpack(res_packet.payload);
			base_stantion.getRover().lat=gps.latitude/1e7f;
			base_stantion.getRover().lon=gps.longitude/1e7f;
			base_stantion.runGUIAction(RunActionEnum.REFRESH_IM_VIEW, null);
			break;
		case msg_mission_current.MAVLINK_MSG_ID_MISSION_CURRENT:
			msg_mission_current cur=new msg_mission_current();
			cur.unpack(res_packet.payload);			
			base_stantion.runGUIAction(RunActionEnum.SET_ACTIVE_PATH_MARK, cur.seq);
			break;
		case msg_heartbeat.MAVLINK_MSG_ID_HEARTBEAT:
			base_stantion.getStatus().setHeartbeatRecivedLastTimeNow();
			msg_heartbeat hb=new msg_heartbeat();
			hb.unpack(res_packet.payload);
			base_stantion.getStatus().clrRoverErrorStatus();
			if(hb.system_status==MAV_STATE.MAV_STATE_CRITICAL)
			{
				base_stantion.getStatus().setRoverErrorStatus();
				String text="������! ���������� �������� ����������. �������� ������ �����";
				base_stantion.runGUIAction(RunActionEnum.SET_ERROR_TEXT, text);				
			}
			break;
		case msg_scaled_imu.MAVLINK_MSG_ID_SCALED_IMU:
			msg_scaled_imu imu=new msg_scaled_imu();
			imu.unpack(res_packet.payload);
			//zmag - move direction in degrees. Value is 0 - on North			
			base_stantion.getRover().setMove_direction(imu.zmag);
			//zacc - head direction in degrees [-180; 180]. Value is 0 - on North
			base_stantion.getRover().setHead_angle(imu.zacc);
			//xacc - body x angle in degrees
			//yacc - body y angle in degrees
			base_stantion.getRover().setBody_angles(imu.xacc, imu.yacc);
			break;
		case msg_sys_status.MAVLINK_MSG_ID_SYS_STATUS:
			msg_sys_status status=new msg_sys_status();
			status.unpack(res_packet.payload);				
			base_stantion.getRover().setBattery_remaining(status.battery_remaining);
			base_stantion.runGUIAction(RunActionEnum.REFRESH_BATTERY_STATUS, null);
			if ((status.onboard_control_sensors_health & MAV_SYS_STATUS_SENSOR.MAV_SYS_STATUS_SENSOR_GPS) != 0)
			{
			   // The bit was set
				base_stantion.getRover().setGps_is_alive(true);
			} else {
				base_stantion.getRover().setGps_is_alive(false);
				String text="���������� ������� GPS-��������� ������ 6. �������������� ������������ ����������";
				base_stantion.runGUIAction(RunActionEnum.SET_ERROR_TEXT, text);		
			}		
			break;			
		case msg_system_time.MAVLINK_MSG_ID_SYSTEM_TIME:
			msg_system_time systime=new msg_system_time();
			systime.unpack(res_packet.payload);
			base_stantion.runGUIAction(RunActionEnum.SET_ROVER_WORK_TIME, systime.time_boot_ms);
			break;
		default:
			break;
		}
	}
	
	public void sendSetModeCommand() throws NoOpenConnectionException
	{
		msg_set_mode mode=new msg_set_mode();
		//MAV_MODE.MAV_MODE_MANUAL_ARMED|DISARMED
		//MAV_MODE.MAV_MODE_AUTO_ARMED|DISARMED
		mode.compid=component_id;
		mode.sysid=sys_id;
		mode.target_system=target_id;
		switch(base_stantion.getStatus().getControlMode())
		{
		case AUTO_DISARMED:
			mode.base_mode=MAV_MODE.MAV_MODE_AUTO_DISARMED;
			break;
		case AUTO_ARMED:
			mode.base_mode=MAV_MODE.MAV_MODE_AUTO_ARMED;
			break;
		case MANUAL_DISARMED:
			mode.base_mode=MAV_MODE.MAV_MODE_MANUAL_DISARMED;
			break;
		case MANUAL_ARMED:
			mode.base_mode=MAV_MODE.MAV_MODE_MANUAL_ARMED;
			break;
		default:
			break;
		}
		MAVLinkPacket pack=mode.pack();
		pack.seq=sequence;
		connector.sendMsg(pack);
		incSeq();	
	}
	
	public void sendManualControlCommand(short left_speed, short right_speed, short head_angle) throws NoOpenConnectionException
	{
		//x - �������� �������� ������ ������ (-1000:1000)
		//y - �������� �������� ������� ������ (-1000:1000)
		//z - ��������� ����� (-180..180)
		msg_manual_control mc=new msg_manual_control();
		mc.compid=component_id;
		mc.sysid=sys_id;
		mc.x=left_speed;
		mc.y=right_speed;
		mc.z=head_angle;
		MAVLinkPacket pack=mc.pack();
		pack.seq=sequence;
		connector.sendMsg(pack);
		incSeq();	
	}
	
	public void sendWaypointCount(int count) throws NoOpenConnectionException
	{
		msg_mission_count mcount=new msg_mission_count();
		mcount.compid=component_id;
		mcount.sysid=sys_id;
		mcount.count=count;
		mcount.target_system=target_id;
		mcount.target_component=target_component;
		MAVLinkPacket pack=mcount.pack();
		pack.seq=sequence;
		connector.sendMsg(pack);
		incSeq();		
	}
	
	public void sendWaypointItem(MapMark m, MapMark obj, int waypoint_id) throws NoOpenConnectionException, InvalidParameterException
	{
		if(m==null || waypoint_id<0)
			throw new InvalidParameterException("MapMark is null or waypoint_id<0");
		msg_mission_item item=new msg_mission_item();
		item.compid=component_id;
		item.sysid=sys_id;
		//x - lat
		//y - lon
		item.x=m.lat;
		item.y=m.lon;
		//param1 obj lat
		//param2 obj lon
		//param3 v
		item.param1=(obj!=null ? obj.lat : -1000);
		item.param2=(obj!=null ? obj.lon : -1000);
		item.param3=m.getSpeed();
		item.seq=waypoint_id;
		item.target_component=target_component;
		item.target_system=target_id;
		MAVLinkPacket pack=item.pack();
		pack.seq=sequence;
		connector.sendMsg(pack);
		incSeq();
	}
	
	
	
	private void incSeq()
	{
		this.sequence++;
	}

}
