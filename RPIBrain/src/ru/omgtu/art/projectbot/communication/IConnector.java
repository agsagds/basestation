package ru.omgtu.art.projectbot.communication;

import com.MAVLink.MAVLinkPacket;

import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;

public interface IConnector {
	public void connect(String port_name, int baud_rate, boolean mavlink_protocol, MsgInterpeter mint);
	public boolean isConnected();
	public void disconnect();
	public void sendMsg(String msg) throws NoOpenConnectionException;
	public void sendMsg(int msg[]) throws NoOpenConnectionException;
	public void sendMsg(MAVLinkPacket msg) throws NoOpenConnectionException;
}
