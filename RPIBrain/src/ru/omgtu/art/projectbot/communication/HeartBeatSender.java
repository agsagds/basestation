package ru.omgtu.art.projectbot.communication;

import ru.omgtu.art.projectbot.exceptions.NoOpenConnectionException;

public class HeartBeatSender extends Thread{
	 
	private int period_mills;
	private volatile boolean active;
	private MsgInterpeter mint;
	
	public HeartBeatSender(int period_seconds, MsgInterpeter mint)
	   {
	      this.period_mills=period_seconds*1000;
	      this.mint=mint;
	   }

	   @Override
	   public void run()
	   {
		   this.active=true;
		   while(active)
		   {
			   try {
				   mint.generateHeartBeat();
			   } catch (NoOpenConnectionException ex) {
				
			   }
			   try {
				   Thread.sleep(period_mills);
			   } catch (InterruptedException ex) {
				   ex.printStackTrace();
			   }
		   }
	   }
	   
	   public void deactivate()
	   {
		   this.active=false;
	   }
}
