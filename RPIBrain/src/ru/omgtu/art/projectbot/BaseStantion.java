package ru.omgtu.art.projectbot;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.omgtu.art.mapapi.Rover;
import ru.omgtu.art.projectbot.communication.HeartBeatSender;
import ru.omgtu.art.projectbot.communication.IConnector;
import ru.omgtu.art.projectbot.communication.MsgInterpeter;
import ru.omgtu.art.projectbot.communication.UARTRadio;
import ru.omgtu.art.projectbot.graphic.FXRunner;
import ru.omgtu.art.projectbot.graphic.IFXController;
import ru.omgtu.art.projectbot.graphic.RunActionEnum;

public class BaseStantion extends Application implements IBaseStantion{
	private MsgInterpeter mint;
	private IFXController controller;
	private volatile Rover rover;
	private volatile SystemStatus status;
	private HeartBeatSender hb_sender;
	private volatile MissionData mission_data;
	private WorkConfiguration work_config;
	
	@Override
	public void start(Stage primaryStage) {
					
		try {	
			//get fx controller
			FXMLLoader loader=new FXMLLoader(getClass().getResource("graphic/GUI.fxml"));			
			AnchorPane root = (AnchorPane)loader.load();		
			init(new UARTRadio(), loader.getController());
			//start heartbeat sending
			hb_sender.start();
			//init fx controller		
			controller.init(this);							
			Scene scene = new Scene(root, 1010, 560);			
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setFullScreen(false);
			primaryStage.setResizable(false);
			primaryStage.show();				
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void stop(){
		hb_sender.deactivate();
		mint.getConnector().disconnect();	
		controller.dispose();		
	    System.out.println("Stage is closing");
	}
	
	public static void main(String[] args) {			
		launch(args);
	}
	@Override
	public SystemStatus getStatus() {
		return status;
	}
	@Override
	public Rover getRover() {
		return rover;
	}

	@Override
	public MsgInterpeter getMsgInterpreter() {
		return mint;
	}

	@Override
	public IFXController getFXController() {
		return controller;
	}

	@Override
	public MissionData getMissionData() {
		return mission_data;
	}

	@Override
	public WorkConfiguration getWorkConfig() {
		return work_config;
	}

	@Override
	public void init(IConnector connector, IFXController fxcontroller) {
		//init work configuration
		work_config=new WorkConfiguration();
		//create system status
		status=new SystemStatus();
		//init mission data
		mission_data=new MissionData();
		//create rover
		rover=new Rover(55.026f, 73.29f, false, 0);	
		//set GUI controller
		controller=fxcontroller;
		//create MavMsgInterpreter
		mint=new MsgInterpeter(111, 0, 0, (short)22, (short)200, this, connector);
		//create heart beat sender
		hb_sender=new HeartBeatSender(1, mint);
	}

	@Override
	public void runGUIAction(RunActionEnum action, Object parameter) {
		Platform.runLater(new FXRunner(action, controller, parameter));
	}

}
