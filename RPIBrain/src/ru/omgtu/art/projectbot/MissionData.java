package ru.omgtu.art.projectbot;

import java.util.LinkedList;
import java.util.List;

import ru.omgtu.art.mapapi.MapMark;
import ru.omgtu.art.mapapi.PathMark;

public class MissionData {
	private volatile List<PathMark> path_marks;
	private volatile List<MapMark> obj_marks;
	private volatile MapMark home_mark;
	
	public MissionData()
	{
		path_marks=new LinkedList<PathMark>();
		obj_marks=new LinkedList<MapMark>();
		setHomeMark(null);
	}

	public List<PathMark> getPathMarks() {
		return path_marks;
	}

	public List<MapMark> getObjMarks() {
		return obj_marks;
	}

	public MapMark getHomeMark() {
		return home_mark;
	}

	public void setHomeMark(MapMark home_mark) {
		this.home_mark = home_mark;
	}
}
