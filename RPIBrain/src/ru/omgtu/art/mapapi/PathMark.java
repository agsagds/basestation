package ru.omgtu.art.mapapi;

public class PathMark extends MapMark {

	private boolean current;
	private boolean accepted;
	
	public PathMark(float _lat, float _lon, boolean _is_obj, int _index) {
		super(_lat, _lon, _is_obj, _index);
		clrAccepted();
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent() {
		this.current = true;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted() {
		this.accepted = true;
		this.current = false;
	}
	
	public void clrAccepted()
	{
		this.accepted = false;
		this.current = false;
	}

	
	

}
