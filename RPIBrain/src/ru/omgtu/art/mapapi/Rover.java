package ru.omgtu.art.mapapi;

public class Rover extends MapMark {

	private float move_direction;
	private float x_body_angle;
	private float y_body_angle;
	private float z_head_angle;
	private float battery_remaining;
	private boolean gps_is_alive;
	
	public Rover(float _lat, float _lon, boolean _is_obj, int _index) {
		super(_lat, _lon, _is_obj, _index);
		move_direction=-180;
		battery_remaining=0;
		setGps_is_alive(false);
	}

	public float getMove_direction() {
		return move_direction;
	}

	public void setMove_direction(float move_direction) {
		this.move_direction = move_direction;
	}
	
	//angle ranges of -180 to 180
	public void setHead_angle(float z_angle) {
		z_head_angle=Math.min(Math.max(-180, z_angle), 180);
	}
	
	public void setBody_angles(float x_angle, float y_angle)
	{
		x_body_angle=x_angle;
		y_body_angle=y_angle;
	}

	public float getX_body_angle() {
		return x_body_angle;
	}

	public float getY_body_angle() {
		return y_body_angle;
	}

	public float getZ_head_angle() {
		return z_head_angle;
	}

	public float getBattery_remaining() {
		return battery_remaining;
	}

	public void setBattery_remaining(float battery_remaining) {
		this.battery_remaining = Math.min(Math.max(0, battery_remaining), 100);
	}

	public boolean gpsIsAlive() {
		return gps_is_alive;
	}

	public void setGps_is_alive(boolean gps_is_alive) {
		this.gps_is_alive = gps_is_alive;
	}
}
