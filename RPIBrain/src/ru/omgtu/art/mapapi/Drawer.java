package ru.omgtu.art.mapapi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ImageObserver;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.text.AttributedCharacterIterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;

import com.sun.javafx.geom.ShapePair;

import javafx.scene.text.Font;

public class Drawer {
	 public static BufferedImage drawMarks(MapMark home_mark, List<PathMark> path_marks, List<MapMark> obj_marks, Frame frame, Rover rover)
     {
		 //cloned image
		 ColorModel cm = frame.getImg().getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = frame.getImg().copyData(null);
	     BufferedImage map = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	     //get Graphics
	     Graphics gr=map.getGraphics();
	     Image home_mark_img=null;
	     Image path_mark_img=null;
	     Image path_mark_current_img=null;
	     Image path_mark_accepted_img=null;
	     Image obj_mark_img=null;
	     Image selected_path_mark_img=null;
	     Image selected_obj_mark_img=null;
	     Image rover_img=null;
	     Image rover_dir_arrow_img=null;
	     Image rover_head_arrow_img=null;	 
	     int x, y;
	     double resize_koff=frame.getZoom()/(double)frame.getMinZoom();
	     try {
	    	 home_mark_img=ImageIO.read(new File("sources\\home_mark.png"));
	    	 path_mark_img = ImageIO.read(new File("sources\\path_mark.png"));
	    	 path_mark_current_img = ImageIO.read(new File("sources\\current_path_mark.png"));
	    	 path_mark_accepted_img = ImageIO.read(new File("sources\\accepted_path_mark.png"));
	    	 selected_path_mark_img = ImageIO.read(new File("sources\\selected_path_mark.png"));
	    	 obj_mark_img = ImageIO.read(new File("sources\\obj_mark.png"));
	    	 selected_obj_mark_img = ImageIO.read(new File("sources\\selected_obj_mark.png"));
	    	 rover_img=ImageIO.read(new File("sources\\rover.png"));
	    	 rover_dir_arrow_img=ImageIO.read(new File("sources\\dir_arrow.png"));
	    	 rover_head_arrow_img=ImageIO.read(new File("sources\\head_arrow.png"));
	    	 
	     } catch (IOException e) {
	    	 // TODO Auto-generated catch block
	    	 e.printStackTrace();
	     } 
	     //draw home mark
	     if(home_mark!=null)
	     {
		     x=home_mark.getX(frame.getZoom(), frame.getLon());
	         y=home_mark.getY(frame.getZoom(), frame.getLat());
	       //  System.out.println("Home mark x: "+x+"; y: "+y);
	         drawImg(gr, x, y, home_mark_img, resize_koff, AnchorEnum.DownMiddle);
	     }
         //draw path marks
         int i=0;
         int pre_x=0, pre_y=0;         
         for(PathMark m : path_marks)
         {
             //������� ��������
           	 x=m.getX(frame.getZoom(), frame.getLon());
             y=m.getY(frame.getZoom(), frame.getLat());
             if(m.getState()==MarkState.DEFAULT)
             {
            	 Image mark_img=path_mark_img;
            	 if(m.isCurrent())
            		 mark_img=path_mark_current_img;
            	 else if(m.isAccepted())
            		 mark_img=path_mark_accepted_img;
            	 drawImg(gr, x, y, mark_img, resize_koff, AnchorEnum.DownMiddle);
             }
             else if(m.getState()==MarkState.SELECTED)
            	 drawImg(gr, x, y, selected_path_mark_img, resize_koff, AnchorEnum.DownMiddle);
             //�������� �������
             gr.setColor(Color.cyan);
             if(i>0)
            	 gr.drawLine(pre_x, pre_y, x, y);            
             //������� ������
             gr.setColor(Color.black);
             gr.setFont(new java.awt.Font("Arial", java.awt.Font.BOLD, 11));
             gr.drawString(StringUtils.center(String.valueOf(i), 3), x-path_mark_img.getWidth(null)/2+path_mark_img.getWidth(null)%2, y-path_mark_img.getHeight(null)/3);
             //�������� ������� � ������
             if(m.getObj()>=0)
             {
            	 gr.setColor(Color.yellow);
            	 int obj_x=obj_marks.get(m.getObj()).getX(frame.getZoom(), frame.getLon());
                 int obj_y=obj_marks.get(m.getObj()).getY(frame.getZoom(), frame.getLat());
                 gr.drawLine(obj_x, obj_y, x, y); 
             }
             //save pre values
             pre_x=x; pre_y=y;
             i++;
         }
         //draw obj marks
         for(MapMark m : obj_marks)
         {
        	 x=m.getX(frame.getZoom(), frame.getLon());
             y=m.getY(frame.getZoom(), frame.getLat());
             if(m.getState()==MarkState.DEFAULT)
            	 drawImg(gr, x, y, obj_mark_img, resize_koff, AnchorEnum.Middle);
             else if(m.getState()==MarkState.SELECTED)
            	 drawImg(gr, x, y, selected_obj_mark_img, resize_koff, AnchorEnum.Middle);   
         }
         //draw rover
         x=rover.getX(frame.getZoom(), frame.getLon());
         y=rover.getY(frame.getZoom(), frame.getLat());
         //���������
         drawImg(gr, x, y, rover_img, resize_koff, AnchorEnum.DownMiddle);
         //����������� �������� | Drawing the rotated image at the required drawing locations
         AffineTransform tx = AffineTransform.getRotateInstance( Math.toRadians (rover.getMove_direction()), rover_dir_arrow_img.getWidth(null) / 2, rover_dir_arrow_img.getHeight(null) / 2);
         AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
         drawImg(gr, x, y, op.filter((BufferedImage) rover_dir_arrow_img, null), resize_koff, AnchorEnum.Middle);
         //����������� ������
         tx = AffineTransform.getRotateInstance( Math.toRadians (rover.getZ_head_angle()), rover_head_arrow_img.getWidth(null) / 2, rover_head_arrow_img.getHeight(null) / 2);
         op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
         drawImg(gr, x, y, op.filter((BufferedImage) rover_head_arrow_img, null), resize_koff, AnchorEnum.Middle);
         gr.dispose();
         return map;
     }
	 
	 enum AnchorEnum{
		 DownMiddle,
		 Middle
	 };
	 
	 private static void drawImg(Graphics gr, int x, int y, Image img, double resize_koff, AnchorEnum anchor)
	 {		 
		 int h=(int)(img.getHeight(null)*resize_koff);
		 int w=(int)(img.getWidth(null)*resize_koff);
		 switch(anchor)
		 {
		 case DownMiddle:
			 x=x-w/2-w%2;
			 y=y-h;
			 break;
		 case Middle:
			 x=x-w/2-w%2;
			 y=y-h/2-h%2;;
			 break;
		 default:
			 break;
		 }
		 		 
		 gr.drawImage(img, x, y, w, h, null);
	 }
}
