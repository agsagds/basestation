package mapapi;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BaseStantion extends Application{
	
	@Override
	public void start(Stage primaryStage) {
					
		try {	
			//get fx controller
			FXMLLoader loader=new FXMLLoader(getClass().getResource("GUI.fxml"));			
			AnchorPane root = (AnchorPane)loader.load();	
			FXController controller=loader.getController();		
			controller.init();
			Scene scene = new Scene(root, 1000, 560);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();				
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void stop(){

	    System.out.println("Stage is closing");
	}
	
	public static void main(String[] args) {		
		Frame f=new Frame(480, 480, "NetFrame.png");
		//73.2925,55.025556,18
		f.setLat(56.025556f);
		f.setLon(74.2925f);
		f.setZoom(19);
		float step_lat=5*(1e-5f);
		float step_lon=5*(1e-5f);
		int count=5;
		//System.out.println(MapAPI.getMarksNetUrl(f, step_lat, step_lon, count));
		//return;
		launch(args);
	}
	
}
