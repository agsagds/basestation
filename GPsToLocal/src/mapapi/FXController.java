package mapapi;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;

public class FXController {	
	private volatile Frame frame;	
	private MapMark mark;
	private volatile MapMark selected_path_mark;
	private volatile int selected_obj_mark=-1;
	private boolean wasd[];	
	@FXML
	private AnchorPane auto_pane;
	@FXML
	private AnchorPane manual_pane;
	@FXML
	private ImageView im_view;
	@FXML
	private TableView<MapMark> tbv_marks;
	@FXML
	private ChoiceBox<String> chb_mark_value;
	@FXML
	private ChoiceBox<String> chb_obj_type;
	@FXML
	private Button bt_connect;
	@FXML
	private Button bt_load;
	@FXML
	private Button bt_remove;
	@FXML
	private Button bt_send;
	@FXML
	private Button bt_left;
	@FXML
	private Button bt_right;
	@FXML
	private Button bt_up;
	@FXML
	private Button bt_down;
	@FXML
	private Button bt_set_value;
	@FXML
	private Button bt_auto_mode;
	@FXML
	private Button bt_manual_mode;
	@FXML
	private CheckBox cb_active;
	@FXML
	private CheckBox cb_return_path_repeat;
	@FXML
	private TextField tb_connector_port;
	@FXML
	private TextField tb_long;
	@FXML
	private TextField tb_lat;
	@FXML
	private TextField tb_value;
	@FXML
	private Slider sl_zoom;
	@FXML
	private Slider sl_speed;
	@FXML
	private Slider sl_head;
	@FXML
	private Label lb_info;
	@FXML
	private Label lb_error;
	@FXML
	private Label lb_time;
	@FXML
	private ProgressBar pb_battery;
	@FXML
	private ProgressIndicator pi_hb;

	public void init() {		
		//init frame
		frame=new Frame(480, 480, "frame.png");
		frame.setLat(55.026472f);
        frame.setLon(73.291216f);
        frame.setZoom(15);      	
        
		//init TableView
		configureTableView();		
		//init choise box mark value items
		ObservableList<String> choises=FXCollections.observableArrayList();
		choises.add("��������");
		choises.add("�������");
		choises.add("������");
		chb_mark_value.setItems(choises);
		chb_mark_value.setValue(chb_mark_value.getItems().get(0));
		//init choise box object type items
		choises=FXCollections.observableArrayList();
		choises.add("�������� �������");
		choises.add("����� ����");
		choises.add("������ ��������");
		choises.add("�����");
		chb_obj_type.setItems(choises);
		chb_obj_type.setValue(chb_obj_type.getItems().get(0));
	}
	
	@FXML
	protected void bt_load_click(ActionEvent event) throws IOException {
		frame.setZoom((int)sl_zoom.getValue());
		try{
	        frame.setLat(Float.valueOf(tb_lat.getText()));
	        frame.setLon(Float.valueOf(tb_long.getText()));
	        frameRefresh();
		} catch (NumberFormatException e) {
			setLabelText("������! �������� ������ ������", true);
		}        
	}
	
	@FXML
	protected void bt_navigation_click(ActionEvent event) throws IOException {
		
		Button s=(Button)event.getSource();
		switch(s.getId())
		{
		case "bt_up":
			frame.setLat(frame.getLat() + MapAPI.yToLat(120, frame.getZoom()));
			break;
		case "bt_down":
			frame.setLat(frame.getLat() - MapAPI.yToLat(120, frame.getZoom()));
			break;
		case "bt_left":
			frame.setLon(frame.getLon() - MapAPI.xToLon(120, frame.getZoom()));
			break;
		case "bt_right":
			frame.setLon(frame.getLon() + MapAPI.xToLon(120, frame.getZoom()));
			break;
		}
		frameRefresh();
	}
	
	@FXML
	protected void bt_set_value_click(ActionEvent event) throws IOException {
	
		
		int mark_id=tbv_marks.getSelectionModel().getSelectedIndex();
		if(mark_id<0)
			return;
		float value=0;
		try
		{
			value=Float.valueOf(tb_value.getText());
		} catch (NumberFormatException ex){
			setLabelText("������! �������� ������ ������� ������", true);
			return;
		}
		mark_id--;
		switch((String)chb_mark_value.getValue())
		{
		case "��������":
			mark.setSpeed(value);
			break;
		case "�������" :
			mark.lon=value;
			break;
		case "������" :
			mark.lat=value;
			break;
		default:
			System.out.println("Oops! No selection");
			break;		
		}
		refreshImView();
		refreshTableView();
	}
	
	@FXML
	protected void im_view_click(MouseEvent event) throws IOException {
		if(event.getButton()!=MouseButton.PRIMARY || !auto_pane.isVisible())
			return;
		int mid_x = 240, mid_y = 240;
        float mark_lat, mark_lon;
        mark_lat = frame.getLat() + MapAPI.yToLat((int) (mid_y - event.getY()), frame.getZoom());
        mark_lon = frame.getLon() - MapAPI.xToLon((int) (mid_x - event.getX()), frame.getZoom());
        switch((String)chb_obj_type.getValue())
		{
		case "����� ����":
			break;
		case "������ ��������":
			break;
		case "�������� �������":
			mark=new MapMark(mark_lat, mark_lon, false, -1);
			refreshTableView();
			refreshImView();
			setLabelText("�������� ������� ����������� � �����: ("+mark_lat+", "+mark_lon+")", false);
			break;
		default:
			break;
		}     
	}
	
	public void refreshImView()
	{
		setImViewImg(Drawer.drawMarks(mark, frame));
	}
	
	public void setLabelText(String text, boolean is_err)
	{
		if(is_err)
		{
			lb_info.setVisible(false);
			lb_error.setVisible(true);
			lb_error.setText(text);
		} else
		{
			lb_info.setVisible(true);
			lb_error.setVisible(false);
			lb_info.setText(text);
		}
	}
	
	
	private void frameRefresh()
	{
		if(mark!=null)
			frame.setImg((BufferedImage) MapAPI.loadImg(frame, true, mark.lon, mark.lat));
		else
			frame.setImg((BufferedImage) MapAPI.loadImg(frame));
		setImViewImg(Drawer.drawMarks(mark, frame));
		setLabelText("Frame ("+frame.getLat()+", "+frame.getLon()+") whith zoom "+frame.getZoom()+" loaded", false);
	}
	
	private synchronized void setImViewImg(BufferedImage img)
	{
		WritableImage wimg=new WritableImage(frame.getW(), frame.getH());
        SwingFXUtils.toFXImage(img, wimg);
        im_view.setImage(wimg);
        //refresh TableView Items
        //refreshTableView();
	}
	
	
	private synchronized void refreshTableView()
	{
		 ObservableList<MapMark> marks=FXCollections.observableArrayList();
		 marks.add(0, mark);
	     tbv_marks.setItems(marks);
	     tbv_marks.refresh();
	}
	
	private void configureTableView()
	{
		tbv_marks.setTableMenuButtonVisible(true);
		
		//tbv_marks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		TableColumn<MapMark, String> indexCol=new TableColumn<MapMark, String>("Id");
		indexCol.setPrefWidth(30);
		indexCol.setMaxWidth(30);
		indexCol.setMinWidth(30);
		indexCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper(String.valueOf(m.getValue().index));
		     }
		     
		  });
		tbv_marks.getColumns().add(indexCol);
		
		TableColumn<MapMark, String> lonCol=new TableColumn<MapMark, String>("Lon");
		lonCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper(String.valueOf(m.getValue().lon));
		     }
		  });
		tbv_marks.getColumns().add(lonCol);
		
		TableColumn<MapMark, String> latCol=new TableColumn<MapMark, String>("Lat");
		latCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper(String.valueOf(m.getValue().lat));
		     }
		  });
		tbv_marks.getColumns().add(latCol);
		
		TableColumn<MapMark, String> speedCol=new TableColumn<MapMark, String>("V, �/�");
		speedCol.setCellValueFactory(new Callback<CellDataFeatures<MapMark, String>, ObservableValue<String>>() {
		     public ObservableValue<String> call(CellDataFeatures<MapMark, String> m) {
		         // p.getValue() returns the Person instance for a particular TableView row
		         return new ReadOnlyObjectWrapper(String.valueOf(m.getValue().getSpeed()));
		     }
		  });
		tbv_marks.getColumns().add(speedCol);
	}
}