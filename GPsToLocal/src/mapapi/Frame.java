package mapapi;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Frame {
	private final int MAXZOOM=20;
	private final int MINZOOM=14;
	private int w;
	private int h;
	private float lat;
	private float lon;
	private BufferedImage img;
	private int zoom;
	private String path;

    public Frame(int _w, int _h, String _path)
    {
        w=_w;
        h=_h;
        path=_path;
        try {
			img=ImageIO.read(new File(path));
		} catch (IOException e) {
			System.out.println("Exception: ���� ����������� �� ���������. Frame.java");
		}
    }

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLon() {
		return lon;
	}

	public void setLon(float lon) {
		this.lon = lon;
	}

	public int getZoom() {
		return zoom;
	}

	public void setZoom(int zoom) {
		this.zoom = Math.max(MINZOOM, Math.min(zoom, MAXZOOM));
	}

	public String getPath() {
		return path;
	}

	public BufferedImage getImg() {
		return img;
	}

	public void setImg(BufferedImage img) {
		this.img = img;
	}
	
	public int getMaxZoom()
	{
		return MAXZOOM;
	}
	
	public int getMinZoom()
	{
		return MINZOOM;
	}

}
