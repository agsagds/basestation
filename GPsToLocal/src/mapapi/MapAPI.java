package mapapi;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class MapAPI {
	private static String apikey = "pk.eyJ1IjoiYWdzYWdkcyIsImEiOiJjazA3dmFzbmswMTBrM2JteHoxOWo5eHp6In0.b4wTi827VoIcEnUBuOTTHg";
    private static String maptype = "digitalglobe.nal0g75k";
    private static String filetype = ".png";
    private static String base_url = "https://api.mapbox.com/v4/";
    private static float base_precision = 0.0001f;
    private static int base_zoom = 14;         
    //4-� ���� ����� ������� = 2[1.125?] ������� ��� ���� 14 (���������� 480�480) �� �����������
    //4-� ���� ����� ������� = 2[2.25?] ������� ��� ���� 15 (���������� 480�480) �� �����������
    //4-� ���� ����� ������� = 5[4.5?] �������� ��� ���� 16 (���������� 480�480) �� �����������
    //4-� ���� ����� ������� = 9 �������� ��� ���� 17 (���������� 480�480) �� �����������
    //4-� ���� ����� ������� = 18 ������� ��� ���� 18 (���������� 480�480) �� �����������
    //������� ��������...
    //5-� ���� ����� ������� = _ �������� ��� ���� 17 (���������� 480�480) �� �����������
    //5-� ���� ����� ������� = _ �������� ��� ���� 18 (���������� 480�480) �� �����������
    //5-� ���� ����� ������� = _ �������� ��� ���� 19 (���������� 480�480) �� �����������
    //5-� ���� ����� ������� = _ �������� ��� ���� 20 (���������� 480�480) �� �����������
    //6-� ���� ����� ������� = _ �������� ��� ���� 17 (���������� 480�480) �� �����������
    //6-� ���� ����� ������� = _ �������� ��� ���� 18 (���������� 480�480) �� �����������
    //6-� ���� ����� ������� = _ �������� ��� ���� 19 (���������� 480�480) �� �����������
    //6-� ���� ����� ������� = _ �������� ��� ���� 20 (���������� 480�480) �� �����������
    private static float[] pix_lons = { 1.17f, 2.325625f, 4.65125f, 9.3f, 18.6f, 37.5f, 75.7f };
    //4-� ���� ����� ������� = 2 ������� ��� ���� 14 (���������� 480�480) �� ���������
    //4-� ���� ����� ������� = 4 ������� ��� ���� 15 (���������� 480�480) �� ���������
    //4-� ���� ����� ������� = 8 �������� ��� ���� 16 (���������� 480�480) �� ���������
    //4-� ���� ����� ������� = 16 �������� ��� ���� 17 (���������� 480�480) �� ���������
    //4-� ���� ����� ������� = 32 ������� ��� ���� 18 (���������� 480�480) �� ���������
    //������� ��������...   
    //5-� ���� ����� ������� = _ �������� ��� ���� 17 (���������� 480�480) �� ���������
    //5-� ���� ����� ������� = _ �������� ��� ���� 18 (���������� 480�480) �� ���������
    //5-� ���� ����� ������� = _ �������� ��� ���� 19 (���������� 480�480) �� ���������
    //5-� ���� ����� ������� = _ �������� ��� ���� 20 (���������� 480�480) �� ���������
    //6-� ���� ����� ������� = _ �������� ��� ���� 17 (���������� 480�480) �� ���������
    //6-� ���� ����� ������� = _ �������� ��� ���� 18 (���������� 480�480) �� ���������
    //6-� ���� ����� ������� = _ �������� ��� ���� 19 (���������� 480�480) �� ���������
    //6-� ���� ����� ������� = _ �������� ��� ���� 20 (���������� 480�480) �� ���������
        
    private static float[] pix_lats = { 2, 4, 8, 16, 32, 64, 128 };
    
    public static Image loadImg(Frame frame)
    {
    	return loadImg(frame, false, 0, 0);
    }
    
    public static Image loadImg(Frame frame, boolean draw_mark, float mark_lon, float mark_lat)
    {
        String url = base_url + maptype + '/';
        if(draw_mark)
            url+=getMarker(mark_lon, mark_lat) + '/';
        url += xyzToString(frame.getLon(), frame.getLat(), frame.getZoom()) + '/';
        url += frame.getW() + "x" + frame.getH() + filetype + "?access_token=" + apikey;
        URL w_url;
        Image im=null;
		try {
			w_url = new URL(url);
			im=ImageIO.read(w_url);
			File outputfile = new File(frame.getPath());
			ImageIO.write((RenderedImage) im, "png", outputfile);
		} catch (IOException e) {
			System.out.println("Exception: MapAPI request timed out");
			try {
				im=ImageIO.read(new File(frame.getPath()));
				return im;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return im;
        //example
        //https://api.mapbox.com/v4/digitalglobe.nal0g75k/45,10,10/480x480.png?access_token=pk.eyJ1IjoiZGlnaXRhbGdsb2JlIiwiYSI6ImNpc2Jtdnh4czAxNTcyenBuYTFwaDR5MTAifQ.49OgdkFD9s3zPSeATsQEPw
        //https://api.mapbox.com/v4/{mapid}/{lon},{lat},{z}/{width}x{height}.{format}?access_token=your-access-token

    }

    public static String getMarksNetUrl(Frame frame, float step_lat, float step_lon, int count)
    {
    	String url = base_url + maptype;
    	float lat, lon;
    	url+='/';
    	//left up
    	lat=frame.getLat();
        for(int i=0;i<=count;i++)
        {
        	lon=frame.getLon();
        	for(int j=0;j<=count;j++)
        	{
        		if(!(i==0 && j==0))
        			url+=',';
        		url+=getMarker(lon, lat);
        		lon+=step_lon;
        	}
        	
        	lat+=step_lat;
        }
        //left down
        lat=frame.getLat()-step_lat;
        for(int i=0;i<count;i++)
        {
        	lon=frame.getLon()+step_lon;
        	for(int j=0;j<count;j++)
        	{
        			url+=',';
        		url+=getMarker(lon, lat);
        		lon+=step_lon;
        	}
        	
        	lat-=step_lat;
        }
        //right up
        lat=frame.getLat()+step_lat;
        for(int i=0;i<count;i++)
        {
        	lon=frame.getLon()-step_lon;
        	for(int j=0;j<count;j++)
        	{
        			url+=',';
        		url+=getMarker(lon, lat);
        		lon-=step_lon;
        	}
        	
        	lat+=step_lat;
        }
        //right down
        lat=frame.getLat();
        for(int i=0;i<=count;i++)
        {
        	lon=frame.getLon();
        	for(int j=0;j<=count;j++)
        	{
        		if(!(lat==frame.getLat() && lon==frame.getLon()))
        		{	
        			url+=',';
        			url+=getMarker(lon, lat);
        		}
        		lon-=step_lon;
        	}
        	
        	lat-=step_lat;
        }
        
        url+='/';
        url += xyzToString(frame.getLon(), frame.getLat(), frame.getZoom()) + '/';
        url += frame.getW() + "x" + frame.getH() + filetype + "?access_token=" + apikey;
        return url;
    }
    
    public static int lonToX(float lon, int zoom)
    {
        return (int)(0.5+(lon*pix_lons[zoom-base_zoom])/base_precision);
    }

    public static int latToY(float lat, int zoom)
    {
        return (int)(0.5 + (lat * pix_lats[zoom - base_zoom]) / base_precision);
    }

    public static float xToLon(int x, int zoom)
    {
        return (base_precision / pix_lons[zoom - base_zoom]) * x;
    }

    public static float yToLat(int y, int zoom)
    {
        return (base_precision / pix_lats[zoom - base_zoom]) * y;
    }

    private static String getMarker(float _lon, float _lat)
    {
        //info https://mapsapidocs.digitalglobe.com/docs/overlay
        //example https://api.mapbox.com/v4/mapbox.streets/pin-l-park+482(-73.975,40.767)/-73.975,40.767,17/500x300.png?access_token={apikey}
    	String[] markers = { "pin-l", "pin-m", "pin-s" };
    	String label = "0";
    	String color = "482";
        return markers[2] + '-' + label + '+' + color + '(' + String.valueOf(_lon).replace(',', '.') + ',' + String.valueOf(_lat).replace(',', '.') + ')';
    }

    private static String xyzToString(float _lon, float _lat, int _zoom)
    {
    	String sx, sy;
        sx = String.valueOf(_lon).replace(',', '.');
        sy = String.valueOf(_lat).replace(',', '.');
        return sx + ',' + sy + ',' + _zoom;
    }
}
