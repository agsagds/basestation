package mapapi;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Drawer {
	 public static BufferedImage drawMarks(MapMark mark, Frame frame)
     {
		 //cloned image
		 ColorModel cm = frame.getImg().getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = frame.getImg().copyData(null);
	     BufferedImage map = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	     //get Graphics
	     Graphics gr=map.getGraphics();
	     Image home_mark_img=null;
	     Image path_mark_img=null;
	     Image path_mark_current_img=null;
	     Image path_mark_accepted_img=null;
	     Image obj_mark_img=null;
	     Image selected_path_mark_img=null;
	     Image selected_obj_mark_img=null;
	     Image rover_img=null;
	     Image rover_dir_arrow_img=null;
	     Image rover_head_arrow_img=null;	 
	     int x, y;
	     double resize_koff=frame.getZoom()/(double)frame.getMinZoom();
	     try {
	    	 home_mark_img=ImageIO.read(new File("sources\\home_mark.png"));
	    	 path_mark_img = ImageIO.read(new File("sources\\path_mark.png"));
	    	 path_mark_current_img = ImageIO.read(new File("sources\\current_path_mark.png"));
	    	 path_mark_accepted_img = ImageIO.read(new File("sources\\accepted_path_mark.png"));
	    	 selected_path_mark_img = ImageIO.read(new File("sources\\selected_path_mark.png"));
	    	 obj_mark_img = ImageIO.read(new File("sources\\obj_mark.png"));
	    	 selected_obj_mark_img = ImageIO.read(new File("sources\\selected_obj_mark.png"));
	    	 rover_img=ImageIO.read(new File("sources\\rover.png"));
	    	 rover_dir_arrow_img=ImageIO.read(new File("sources\\dir_arrow.png"));
	    	 rover_head_arrow_img=ImageIO.read(new File("sources\\head_arrow.png"));
	    	 
	     } catch (IOException e) {
	    	 // TODO Auto-generated catch block
	    	 e.printStackTrace();
	     } 
	     //draw home mark
	     if(mark!=null)
	     {
		     x=mark.getX(frame.getZoom(), frame.getLon());
	         y=mark.getY(frame.getZoom(), frame.getLat());
	         drawImg(gr, x, y, home_mark_img, resize_koff, AnchorEnum.DownMiddle);
	     }
         gr.dispose();
         return map;
     }
	 
	 enum AnchorEnum{
		 DownMiddle,
		 Middle
	 };
	 
	 private static void drawImg(Graphics gr, int x, int y, Image img, double resize_koff, AnchorEnum anchor)
	 {
		 int h=(int)(img.getHeight(null)*resize_koff);
		 int w=(int)(img.getWidth(null)*resize_koff);
		 switch(anchor)
		 {
		 case DownMiddle:
			 x=x-w/2-w%2;
			 y=y-h;
			 break;
		 case Middle:
			 x=x-w/2-w%2;
			 y=y-h/2-h%2;;
			 break;
		 default:
			 break;
		 }
		 		 
		 gr.drawImage(img, x, y, w, h, null);
	 }
}
