package mapapi;

public class MapMark {
	public float lat;
	public float lon;
	public int index;
	private float speed;
	private boolean is_obj;
	private int obj;
    private final int mid_position = 240;

    public MapMark(float _lat, float _lon, boolean _is_obj, int _index)
    {
        lat = _lat;
        lon = _lon;
        setIs_obj(_is_obj);  
        obj=-1;
        index=_index;
        setSpeed(1);
    }
    /// <summary>
    /// ���������� X ���������� �� �����������
    /// </summary>
    /// <param name="zoom">������� ���</param>
    /// <param name="mid_lon">������� ������ �����</param>
    /// <returns>X ���������� �� �����</returns> 
    public int getX(int zoom, float mid_lon)
    {
        return mid_position + MapAPI.lonToX(lon - mid_lon, zoom);
    }
    /// <summary>
    /// ���������� Y ���������� �� �����������
    /// </summary>
    /// <param name="zoom">������� ���</param>
    /// <param name="mid_lat">������ ������ �����</param>
    /// <returns>Y ���������� �� �����</returns> 
    public int getY(int zoom, float mid_lat)
    {
        return mid_position - MapAPI.latToY(lat - mid_lat, zoom);
    }
	public int getObj() {
		return obj;
	}
	public void setObj(int obj) {
		this.obj = obj;
	}
	public boolean is_obj() {
		return is_obj;
	}
	public void setIs_obj(boolean is_obj) {
		this.is_obj = is_obj;
	}
	public float getSpeed() {
		return speed;
	}
	public void setSpeed(float speed) {
		this.speed = Math.max(Math.min(2, speed), 0.5f);
	}
}
