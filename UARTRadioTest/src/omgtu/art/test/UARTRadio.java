package omgtu.art.test;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TooManyListenersException;
import java.util.logging.Logger;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Parser;
import com.MAVLink.common.msg_mission_count;

import gnu.io.NRSerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
public class UARTRadio implements SerialPortEventListener, IConnector {
	
	private final Logger log = Logger.getLogger("UART Radio logger");
    
    private boolean mavlink_protocol;

    private NRSerialPort serial;
    


    
    public void connect(String port_name, int baud_rate, boolean mavlink_protocol)  {    	
    	
    	this.mavlink_protocol=mavlink_protocol;
        log.info("SerialConnection PostConstruct callback: connecting to Serial...");
        
        serial = new NRSerialPort(port_name, baud_rate);
        serial.connect();

        try {
			serial.addEventListener(this);
		} catch (TooManyListenersException e) {
			log.warning("TooManyListenersException");
		}
        
        if (serial.isConnected()) {
            log.info("Uart connection opened!");
        }
    }

   
    public void disconnect() {
        log.info("SerialConnection PreDestroy callback: disconnecting from Serial...");

        if (serial != null && serial.isConnected()) {
            serial.disconnect();

            if (!serial.isConnected()) {
                log.info("Arduino connection closed!");
            }
        }
    }
    
    public synchronized void sendMsg(String msg) throws NoOpenConnectionException
    {
    	if(!isConnected())
    		throw new NoOpenConnectionException();
    	DataOutputStream outs = new DataOutputStream(serial.getOutputStream());
    	try {
			outs.write(msg.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.warning(e.getMessage());
		}
    }
    
    public synchronized void sendMsg(int msg[])throws NoOpenConnectionException
    {
    	if(!isConnected())
    		throw new NoOpenConnectionException();
    	DataOutputStream outs = new DataOutputStream(serial.getOutputStream());
    	try {
    		
    		for(int i=0; i<msg.length;i++)
    		{
    			outs.write(msg[i]);
    			outs.flush();
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.warning(e.getMessage());
		}
    }
    
    public synchronized void sendMsg(MAVLinkPacket msg)throws NoOpenConnectionException
    {
    	if(!isConnected())
    		throw new NoOpenConnectionException();
    	DataOutputStream outs = new DataOutputStream(serial.getOutputStream());
    	try {   		
    		outs.write(msg.encodePacket());
    		System.out.println("Sended: "+msg.unpack().toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.warning(e.getMessage());
		}
    }


	@Override
	public void serialEvent(SerialPortEvent arg0) {
		if(!mavlink_protocol)
		{
			BufferedReader br=new BufferedReader(new InputStreamReader(serial.getInputStream()));
			try {
				
				System.out.println(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
				log.warning(e.getMessage());
			}
		} else {
			MAVLinkPacket mrpack=null;            
			DataInputStream ins = new DataInputStream(serial.getInputStream());
	        Parser mpars=new Parser();
			String message;
	            //�������� ��������� �� ��
				int saver[]=new int[260];
				int j=0;
	            while(true)
	            {	            	
	            	try {
	            		saver[j]=ins.read();
	            		
						mrpack=mpars.mavlink_parse_char(saver[j]);
						j++;
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
						log.warning(e.getMessage());
					}
	            	if(mrpack!=null) break;
	            }
	            if(mrpack.msgid==msg_mission_count.MAVLINK_MSG_ID_MISSION_COUNT)
	            {
	            	msg_mission_count count=new msg_mission_count();
	            	count.unpack(mrpack.payload);
	            	Main.count=count.count;
	            }
	            log.info("MP Received: " + mrpack.unpack().toString());
		}
		
	}


	@Override
	public boolean isConnected() {
		if(serial==null)
			return false;
		return serial.isConnected();
	}
}
