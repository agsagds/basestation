package omgtu.art.test;

import com.MAVLink.MAVLinkPacket;

public interface IConnector {
	public void connect(String port_name, int baud_rate, boolean mavlink_protocol);
	public boolean isConnected();
	public void disconnect();
	public void sendMsg(String msg) throws NoOpenConnectionException;
	public void sendMsg(int msg[]) throws NoOpenConnectionException;
	public void sendMsg(MAVLinkPacket msg) throws NoOpenConnectionException;
}
