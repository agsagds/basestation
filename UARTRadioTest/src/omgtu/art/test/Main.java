package omgtu.art.test;

import com.MAVLink.common.msg_heartbeat;
import com.MAVLink.common.msg_mission_ack;
import com.MAVLink.common.msg_mission_request;
import com.MAVLink.enums.MAV_MISSION_RESULT;

public class Main {

	public static int count=0;
	
	public static void main(String[] args) throws NoOpenConnectionException, InterruptedException {
		UARTRadio obj=new UARTRadio();
		String port_name="COM5";
		int baud_rate=57600;
		boolean mavlink_protocol=true;
		obj.connect(port_name, baud_rate, mavlink_protocol);
		boolean ack=false;
		while(true)
		{
			
			if(count>0)
			{
				msg_mission_request req=new msg_mission_request();
				req.seq=count-1;
				obj.sendMsg(req.pack());
				if(count==1)
					ack=true;
				count--;
				
			}
			if(ack)
			{
				ack=false;
				msg_mission_ack msg=new msg_mission_ack();
				msg.type=MAV_MISSION_RESULT.MAV_MISSION_ACCEPTED;
				obj.sendMsg(msg.pack());
			}
			//msg_heartbeat hb=new msg_heartbeat();
			//obj.sendMsg(hb.pack());
			//Thread.sleep(100);
		}
	}

}
